import { FivethreeCoreModule } from '@fivethree/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MarkdownModule } from 'ngx-markdown';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LottieAnimationViewModule } from 'ng-lottie';
import { UtilService } from '@services/util.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { IonicStorageModule } from '@ionic/storage';
import { AndroidFullScreen } from '@ionic-native/android-full-screen/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
// import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
export function playerFactory() {
  return player;
}
import { FivFeatureDiscoveryModule } from '@fivethree/core';
import { FivIconModule } from '@fivethree/core';
// import { Contacts} from '@ionic-native/contacts/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FivethreeCoreModule,
    IonicModule.forRoot({mode:'ios'}),
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    MarkdownModule.forRoot({ loader: HttpClient }),
    LottieAnimationViewModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    }),
    FivFeatureDiscoveryModule,
    FivIconModule,
  ],
  providers: [
    SocialSharing,
    StatusBar,
    SplashScreen,
    OneSignal,
    LocationAccuracy ,
    AndroidFullScreen,
    CallNumber, 
  

 { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
   // { provide: LocationStrategy, useClass: HashLocationStrategy },

    UtilService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
