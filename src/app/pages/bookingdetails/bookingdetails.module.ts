import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FivStepperModule } from '@fivethree/core';

import { IonicModule } from '@ionic/angular';

import { BookingdetailsPage } from './bookingdetails.page';

const routes: Routes = [
  {
    path: '',
    component: BookingdetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FivStepperModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BookingdetailsPage]
})
export class BookingdetailsPageModule {}
