import { FivStepper } from '@fivethree/core';
import { AlertController } from '@ionic/angular';
import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { FivDialog } from '@fivethree/core';
import { ModalController,NavController,Platform } from '@ionic/angular';
import { GlobalService } from '../../services/global.service';
import { Storage } from '@ionic/storage';
import { ThanosSnapService } from '@wellwind/ngx-thanos';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-bookingdetails',
  templateUrl: './bookingdetails.page.html',
  styleUrls: ['./bookingdetails.page.scss'],
})
export class BookingdetailsPage implements OnInit {

  @ViewChild('stepperV') stepperV: FivStepper;
loaded:any=false;
formdata:any={};
data:any;
orderid:any;
// address=[
//   "payment_method": "bacs", "payment_method_title": "Direct Bank Transfer", "set_paid":"true",
// "billing":[
//   "first_name" : "John",
//       "last_name" : "Doe",
//       "address_1" : "969 Market",
//       "address_2" : "",
//       "city" : "San Francisco",
//       "state" : "CA",
//       "postcode" : "94103",
//       "country" : "US",
//       "email" : "john.doe@example.com",
//       "phone" : "(555) 555-5555"
// ]
// ];
constructor(private activeRoute: ActivatedRoute, router: NavController,public alertController: AlertController,private storage: Storage,private navCtrl: NavController, public global: GlobalService) {}

  ngOnInit() {
    this.orderid = +this.activeRoute.snapshot.paramMap.get('orderid');
    this.getdata();
  }

  ionViewDidEnter() {
    // this.stepperV.select(0);
  }

  getdata(){
  

 this.formdata.userid="1";
 this.formdata.id=this.orderid;
//  this.formdata.distance=this.km.value;
//  this.formdata.fromaddress=this.global.from;
//  this.formdata.toaddress=this.global.to;
//  this.formdata.sharing=this.sharing;
//  this.formdata.things=this.global.items;
//  this.formdata.form=this.global.mappageform;
 
 console.log(this.formdata);

 
  var headers = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json' );
 
 
  let postData =  this.formdata;
 
  this.global.http.post(this.global.url+"estimation/viewsingle.php", postData,{observe: 'response'})
    .subscribe(data => {
      console.log(data.body);
      console.log(data.body['status']);
      // tslint:disable-next-line: triple-equals
      if(data.body['status']=='success'){

        this.data=data.body['records'];
        console.log('records');
        console.log(this.data);

        this.data=this.data[0];
        console.log(this.data.status);


        if(this.data.status=='booked'){
        this.stepperV.select(0);
        }
        if(this.data.status=='inspection'){
        this.stepperV.select(1);
        }
        if(this.data.status=='transit'){
        this.stepperV.select(2);
        }
        if(this.data.status=='delivered'){
        this.stepperV.select(3);
        }
     this.loaded=true;

      }
     }, error => {
      console.log(error);
    });
    
 
   }


}
