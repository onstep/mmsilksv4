import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import {  NavController } from '@ionic/angular';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
  wishid:any;
  orderid:any;
  orderdetails:any;
  productdetail: any;
  data: any;
  loaded:any='0';  
  cart:any;
  email:any="balajikannan@onstep.com";
  itemcount: any;
  products:any;
postdata:any={};
  placeorder: Object;
  price:any;
  paymentmethod:any={};

  // shipping:any={};
  constructor(public loadingController: LoadingController,private activeRoute: ActivatedRoute,public global: GlobalService,private router: Router,private storage: Storage) { }

  ngOnInit() {
    this.postdata.shipping=this.global.useraddress;
    // this.postdata.role=this.global.customerrole;
    this.postdata.role='contributor';
    this.postdata.customer_id=this.global.customerid;
    this.postdata.payment_method='bacs';
    this.postdata.payment_method_title='direct bank transfer';
    this.postdata.set_paid='true';
    
   this.global.storage.get('storecart').then((val) => {
    console.log(val);
    
    var i;
for(i = 0; i < val.length; i++){
  val[i].product_id = val[i]['productid'];
  val[i].quantity = val[i]['count'];
    delete val[i].productid;
    delete val[i].count;
    delete val[i].price;
}

    this.cart=val;
  console.log(this.cart);
    this.postdata.line_items=this.cart;

    console.log( this.postdata.line_items);
 
    this.initial();
    this.global.payment=this.paymentmethod;

     

   
  });
     
      
  }
  getcount(id){
    console.log(id);
    var  index = this.cart.findIndex((obj => obj.product_id == id));
    console.log(index);
    console.log(this.cart);
console.log(this.cart[index].quantity);
return this.cart[index].quantity;
  }
 
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

 //   let idarray=[{id:"5182"},{id:"5205"}]
//  console.log(this.global.storecartdetails.all);
console.log(this.cart);

    let idarray=this.cart;
    console.log(idarray);
    let postData =  { id:idarray }
    console.log(postData);

    this.global.http.post(this.global.url+"totalprice.php", postData,{observe: 'response'})      
      //this.http.get(link)
      
       .subscribe(data => {
      
        this.global.carting= data.body;
        this.price=this.global.carting.price;
        this.products=this.global.carting.product;
       this.global.ordering=  this.global.carting;
        console.log( this.global.ordering);
     
      
      

              
  
  
  this.loaded="1";
       }, error => {
       
       })
  }
  async  createorder()
  {
  // this.loaded="0";
   
    const loading = await this.loadingController.create({
      message: 'Please Wait a Second',
     
    });
    await loading.present();


   // const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
    
    console.log(this.paymentmethod);
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );



    let idarray=this.postdata;
    console.log(idarray);
    let postData =  { id:idarray }
    console.log(postData);

    this.global.http.post(this.global.url+"createorder.php", idarray,{observe: 'response'})      
      //this.http.get(link)
      
       .subscribe(data => {
  
        this.orderdetails= data.body;
        console.log(this.orderdetails);

        this.orderid=this.orderdetails.orderdata.id;
console.log(this.orderid);
//  this.orderid=this.placeorder.orderdata.id;

  // this.loaded="1";
   loading.dismiss();

  this.router.navigateByUrl('/mmsucess/'+this.orderid);

       }, error => {
       
       })
      //  this.splice();
  }
  detail(id){
    this.router.navigateByUrl('/mmproductdetails/'+id);

  }
 
  // splice(){
  //     this.global.storage.get('storecart').then((val) => {
  //       console.log(val[0].productid);
  //       var pid;
  //       for(var i=0;i<val.length; i++){
  //        pid=val[i].productid
  //          }
  //          console.log(pid);
  //        var  index = val.findIndex((obj => obj.productid == pid));
  //        console.log(index);
       
     
    
  //     val.splice(index, 1);
  //     this.item.splice(index, 1);
    
    
  //     console.log(val);
  //     this.storage.set('storecart',val).then((val) => {
  //     console.log(val);
  //   this.ngOnInit();
    
  //     });
    
  //     });
    
     
  //   }
}
