import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-myhome',
  templateUrl: './myhome.page.html',
  styleUrls: ['./myhome.page.scss'],
})
export class MyhomePage implements OnInit {
  titleLayout = 'show';
  position = 'center';
  fabVisible = true;
  icon = 'checkmark';
  

  customTitleLayoutOptions: any = {
    header: 'Title Layout',
    subHeader: 'Select a title layout'
  };

  customFabPositionOptions: any = {
    header: 'Fab Position',
    subHeader: 'Select a floating action button position.'
  };
  cartcount:any;
  orders:any;
  loaded: any;
  constructor(public global: GlobalService,private router: Router) { }

  ngOnInit() {
    console.log(this.global.useraddress);
    this.global.cart='1';
    console.log(this.global.cart);

    // this.orders= this.global.customerid;
    this.orders= this.global.customerid;

  }
  goto(){
    this.router.navigateByUrl('/mmwallet');
  }
 
  
}
