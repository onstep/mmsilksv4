import { ComponentsModule } from '@components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

// import { TabsPageRoutingModule } from './tabs.router.module';
import { FivAppBarModule, FivCenterModule } from '@fivethree/core';


import { MyhomePage } from './myhome.page';

// const routes: Routes = [
//   {
//     path: '',
//     component: MyhomePage
//   }
// ];
const routes: Routes = [
  {
    path: 'tabs',
    component: MyhomePage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren:
              '../../pages/mmhome/mmhome.module#MmhomePageModule'
          }
        ]
      },
      {
        path: 'cart',
        children: [
          {
            path: '',
            loadChildren:
            '../../pages/mmnewcarts/mmnewcarts.module#MmnewcartsPageModule'
          }
        ]
      },
      {
        path: 'category',
        children: [
          {
            path: '',
            loadChildren:
            '../../pages/category/category.module#CategoryPageModule'
          }
        ]
      },
      {
        path: 'account',
        children: [
          {
            path: '',
            loadChildren:
            '../../pages/mmaddress/mmaddress.module#MmaddressPageModule'
          }
        ]
      }, {
        path: 'myorders',
        children: [
          {
            path: '',
            loadChildren:
            '../../pages/mmaddress/mmaddress.module#MmaddressPageModule'
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren:
              '../../pages/mmhome/mmhome.module#MmhomePageModule'
          }
        ]
      },
     
    
      // {
      //   path: '',
      //   redirectTo: '/tabs/tab1',
      //   pathMatch: 'full'
      // }
    ]
  },
  // {
  //   path: '',
  //   redirectTo: '/tabs/tab1',
  //   pathMatch: 'full'
  // }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    FivAppBarModule,
    FivCenterModule
  ],
  declarations: [MyhomePage]
})
export class MyhomePageModule {}
