import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
// import {AddresspickerPage} from '../addresspicker/addresspicker.page';
import { FivFeatureDiscoveryModule } from '@fivethree/core';
import { IonicModule } from '@ionic/angular';
import { LottieAnimationViewModule } from 'ng-lottie';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
export function playerFactory() {
  return player;
}
import { MapPage } from './map.page';
import { FivIfModule } from './../../../../projects/core/src/lib/if-platform/if-platform.module';
import { ComponentsModule } from '@components/components.module';
import { FivBottomSheetModule, FivIconModule,FivBackButtonModule } from '@fivethree/core';
const routes: Routes = [
  {
    path: '',
    component: MapPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    FivBottomSheetModule,
    FivFeatureDiscoveryModule,
    FivIconModule,
    FivBackButtonModule,
    ComponentsModule,
    FivIfModule,
   LottieAnimationViewModule.forRoot(),
    RouterModule.forChild(routes),
  ],
  declarations: [MapPage],
  // entryComponents  : [AddresspickerPage],
})
export class MapPageModule {}
