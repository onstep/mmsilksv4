/// <reference types="@types/googlemaps" />
import { Component, OnInit,ViewChild } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Polyline,
  BaseArrayClass,
  LatLng,
  ILatLng,
  Marker
} from '@ionic-native/google-maps';
import { trigger, style, animate, transition } from '@angular/animations';
import { AlertController } from '@ionic/angular';

import { ToastController } from '@ionic/angular';
import { DrawerState } from '@fivethree/core';
import { ModalController,NavController,Platform } from '@ionic/angular';
// import {AddresspickerPage} from '../addresspicker/addresspicker.page';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../services/global.service'
declare var require: any
const decodePolyline = require('decode-google-map-polyline');
//import { decodePolyline } from  '../../../../node_modules/decode-google-map-polyline'
import { ComponentsModule } from '@components/components.module';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateY(100%)', opacity: 0}),
          animate('800ms', style({transform: 'translateY(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateY(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateY(100%)', opacity: 0}))
        ])
      ]
    )
  ],
})
export class MapPage implements OnInit {
  map: GoogleMap;
  shouldBounce = true;
  dockedHeight = 224;
  distanceTop = 0;
  drawerState;
  show=0;
  backbutton=0;
  states = DrawerState;
@ViewChild('fab')fab;
@ViewChild('drop')drop;
  handle = true;
  float = true;
  rounded = true;
  ready=0;
  modal;
  address:any={};
  fromloaded:any=0;
  dstate:any=0;
  toloaded:any=0;
  arr:any;
  fp = 20;
  co = 20;
  km ;
  selected ='home';
  formdata:any={};
  selectedtruckid=null;
  lottieConfig;

 directionsService = new google.maps.DirectionsService();

 constructor (public alertController: AlertController,public toastController: ToastController,public global:GlobalService,private storage: Storage,private platform: Platform,private navCtrl: NavController,private modalController:ModalController) { 
    platform.ready().then((readySource) => {
      console.log('Width: ' + platform.width());
      console.log('Height: ' + platform.height());
      this.dockedHeight = platform.height()*22/100;
console.log(this.dockedHeight);
this.ready=1;
this.drawerState=DrawerState.Docked;

this.storage.get('mappageform').then((mappageform) => {
  if(mappageform){
    console.log('form'+mappageform);
    console.log(mappageform);
    this.formdata=mappageform;
  }
});
this.formdata=this.global.mappageform;
    });

  }
  changeit(){
    this.show = this.show === 1 ? 0 : 1;
  }
  async pickupcontact() {
    var name;
    var mobile;
    if(this.formdata.pickupcontact){
       name=this.formdata.pickupcontact.name;
    }
    if(this.formdata.pickupcontact){
       mobile=this.formdata.pickupcontact.mobile;
    }
    const alert = await this.alertController.create({
      header: 'Pickup Contact',
      inputs: [
      
        {
          name: 'name',
          type: 'text',
          value: name,
          placeholder: 'Name'
        },
      
        // input date with min & max
      
        {
          name: 'mobile',
          type: 'number',
          value: mobile,
          placeholder: 'Mobile',
          min: 10,
          max: 10
        },
       
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (val) => {
            console.log(val);
          }
        }, {
          text: 'Ok',
          handler: (val) => {
            console.log(val);

            if(val.name && val.mobile){
              this.formdata.pickupcontact={};
          this.formdata.pickupcontact.name=val.name;     
            this.formdata.pickupcontact.mobile=val.mobile;  
            console.log('ss')
            }
     
               }
        }
      ]
    });

    await alert.present();
  }
  async dropcontact() {
    var name;
    var mobile;
    if(this.formdata.dropcontact){
       name=this.formdata.dropcontact.name;
    }
    if(this.formdata.dropcontact){
       mobile=this.formdata.dropcontact.mobile;
    }
    const alert = await this.alertController.create({
      header: 'Drop Contact details',
      inputs: [
      
        {
          name: 'name',
          type: 'text',
          value: name,
          placeholder: 'Name'
        },
      
        // input date with min & max
      
        {
          name: 'mobile',
          type: 'number',
          value: mobile,
          placeholder: 'Mobile',
          min: 10,
          max: 10
        },
       
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (val) => {
            console.log(val);
          }
        }, {
          text: 'Ok',
          handler: (val) => {
            console.log(val);

            if(val.name && val.mobile){
              this.formdata.dropcontact={};
          this.formdata.dropcontact.name=val.name;     
            this.formdata.dropcontact.mobile=val.mobile;  
            console.log('ss')
            }
     
               }
        }
      ]
    });

    await alert.present();
  }

  async ngOnInit() {
    this.lottieConfig = {
            path: 'assets/cc.json',
           renderer: 'canvas',
            autoplay: true,
            loop: true
        };

    // Since ngOnInit() is executed before `deviceready` event,
    // you have to wait the event.
    await this.platform.ready();

    this.storage.get('fromaddress').then((val) => {
      console.log(val);
    this.global.from=val;
    if(this.global.from){
      this.fromloaded=1;
    }
      //  this.route();
  
      //  this.route();

    });
    this.storage.get('toaddress').then((val) => {
      console.log(val);
      this.global.to=val;
  
      //  this.route();
    if(this.global.to){
      this.toloaded=1;
      if(this.global.from && this.global.to){
        this.route();
      }
    }
    

      //  this.route();

    });
    console.log(this.address);

    await this.loadMap();
  }
  async toast(text,header) {
    const toast = await this.toastController.create({
      message: text,
      color:'danger',
      header: header,
      duration: 3000
    });
    toast.present();
  }

 
 async change(){
  if(this.drawerState===DrawerState.Docked){
    this.dstate='docked';
  }
  if(this.drawerState===DrawerState.Top){
    if(!this.global.from){
      this.fab.show();
      
      await this.delay(5);

      this.drawerState=DrawerState.Docked;
  
      this.dstate='docked';
return;
    }
    if(!this.global.to){
      this.drop.show();
      await this.delay(5);

      this.drawerState=DrawerState.Docked;
  
      this.dstate='docked';

return;
    }
    this.dstate='top';

  }
  if(this.drawerState===DrawerState.Bottom){
    await this.delay(5);

    this.drawerState=DrawerState.Docked;

    this.dstate='docked';

  }
  console.log(this.drawerState);
  console.log(this.dstate);

}

  all(){
    //alert();
  }
       truckselect(truckid){
            this.selectedtruckid=truckid;

             }

      next(){
        if(!this.formdata.pickupfloor){
          this.toast("Please select floor in pickup address!","");
        return;
        }
        if(!this.formdata.pickupelevator){
          this.toast("Please select service elevator of pickup address !","");
        return;
        }
        if(!this.formdata.pickupdate){
          this.toast("Please select pickup date !","");
        return;
        }
        if(!this.formdata.pickupappointmentdate){
          this.toast("Please select Appointment date !","");
        return;
        }
      
        if(!this.formdata.dropfloor){
          this.toast("Please select floor in drop address!","");
        return;
        }
        if(!this.formdata.pickupelevator){
          this.toast("Please select service elevator of pickup address !","");
        return;
        }
        if(!this.formdata.pickupelevator){
          this.toast("Please select service elevator of pickup address !","");
        return;
        }
        if(this.formdata.pickupcontact  ){

        if(!this.formdata.pickupcontact.mobile || !this.formdata.pickupcontact.mobile  ){
          this.toast("Please fill pickup contact details !","");
          return;

        }
      }else{
        this.toast("Please fill pickup contact details !","");
          return;

      }
      if(this.formdata.dropcontact  ){
  if(!this.formdata.dropcontact.mobile || !this.formdata.dropcontact.mobile    ){
          this.toast("Please fill drop contact details !","");
          return;
        }
      }else
      {
        this.toast("Please fill drop contact details !","");
        return;
      }
 
        

        this.global.mappageform=this.formdata;
        console.log(this.global.mappageform);
        this.storage.set('mappageform',this.formdata);

        console.log(this.global);
      this.navCtrl.navigateForward('/things')
      }
      truckt(){
        

        this.global.mappageform=this.formdata;
        console.log(this.global.mappageform);
        this.storage.set('mappageform',this.formdata);
        this.storage.set('selectedtruck',this.selectedtruckid);
if(this.selectedtruckid==null){
  this.toast('Please select the truck ','');
  return;
}
        console.log(this.global);
      this.navCtrl.navigateForward('/confirmtruck')
      }

  up(){
    this.drawerState=DrawerState.Top;
    this.dstate='top';

  }
  down(){
    this.drawerState=DrawerState.Docked;
    this.dstate='docked';

  }

  home(){

    if(!this.global.from){
      this.fab.show()
return;
    }
    if(!this.global.to){
      this.drop.show()

return;
    }
this.selected='home';
this.up();
  }
  truck(){
    
    if(!this.global.from){
      this.fab.show()
return;
    }
    if(!this.global.to){
      this.drop.show()

return;
    }
this.selected='truck';
this.up();
  }
  nexo(){
  this.navCtrl.navigateForward('/success');
  }
  office(){
    
    if(!this.global.from){
      this.fab.show()
return;
    }
    if(!this.global.to){
      this.drop.show()

return;
    }
this.selected='office';
this.up();
  }
  async route(){
  // alert();
   let arr;
    let from = {lat: this.global.from.lat, lng: this.global.from.lng};
    let to = {lat: this.global.to.lat, lng: this.global.to.lng};
    var request = {
      origin: from,
      destination: to,
      travelMode: google.maps.TravelMode.WALKING
    };
   await this.directionsService.route(request, (response, status) => {
      
   console.log(response);
   console.log(response.routes[0].legs[0].distance);
   this.km=response.routes[0].legs[0].distance;
   this.storage.set('km',response.routes[0].legs[0].distance);

      console.log(status);

      if (status === google.maps.DirectionsStatus.OK) {
       // alert();
     //  console.log(response.routes[0].overview_polyline);
       arr=response.routes[0].overview_polyline;
        this.arr= decodePolyline(arr);
       
       
       this.alo();

      }
});
console.log(this.map);
return;
      this.map.animateCamera({
        'target': arr,
        'tilt': 60,
        'zoom': 18,
        'bearing': 140,
        'duration': 2000 // = 5 sec.
      }).then(() => {
        console.log("The animation is done");

        let polyline: Polyline = this.map.addPolylineSync({
          points: arr,
          color: '#AA00FF',
          width: 10,
          geodesic: true,
          clickable: true , // clickable = false in default
          duration: 2000 // = 5 sec.
    
        });

      });
  

  }
 async alo(){
  for(let y=0;y<this.arr.length;y++){
  //  console.log(this.arr[y]);
   

          }
  //  console.log('this.arr');
    console.log(this.arr);
  //  alert();
  this.map.clear();

    this.map.animateCamera({
      'target': this.arr,
      'duration': 2000 // = 5 sec.
    }).then(() => {
      console.log("The animation is done");

      let zoom=this.map.getCameraZoom();
      this.addmarker();
     // alert(zoom);
      this.map.animateCamera({
        'target': this.arr,
        'zoom': zoom-4,
        'duration': 1000 // = 5 sec.
      }).then(async () => {
        this.map.animateCameraZoomOut();
       // this.map.animateCameraZoomIn(zoom-1);
      // alert(this.map.getCameraZoom());

      //  this.map.addMarkerSync({})
      let f ;
      let t ;
      let ft ;
      for(let y=0;y<this.arr.length-1;y++){
        f = this.arr[y];
        t = this.arr[y+1];
        ft = [
          f,
          t
        ];
        

        
        console.log(this.arr[y]);

        let polyline: Polyline = this.map.addPolylineSync({
          points: ft,
          color: '#e2e841',
          width: 5,
          geodesic: true,
          clickable: true , // clickable = false in default
    
        });
        await this.delay(5);
      }
              

      });
    

    });
  }
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}
ionViewWillEnter(){
  this.ngOnInit();
}
openpicker(type){
  var navOptions = {
    animated: true,
    easing:'DSFF'
};
  this.navCtrl.navigateForward('/mappicker/'+type,navOptions)


}

  async presentModal(type) {
    console.log(type);
    let modal = await this.modalController.create({
      component: 'AddresspickerPage',
      componentProps: {
        'type': type,
      }
    });
  modal.present();
  const { data } = await modal.onWillDismiss();
console.log(data);
//var data =dataa;
/*
if(data.type==='from'){
  var me=this;
  console.log(me.address);
  me.address.from=data;
  this.fromloaded=1;
}
if(data.type==='to'){
  var me=this;
  me.address.to=data;
  this.toloaded=1;

}
*/
console.log('ravi');
console.log(this.global.from);
if(this.global.to){
  this.toloaded=1;
}
if(this.global.from){
  this.fromloaded=1;
}
if(this.global.from && this.global.to){
this.route();
}


this.storage.set('fromaddress',this.global.from);
this.storage.set('toaddress',this.global.to);

  }


  loadMap() {
    console.log(this.global.mapstyle);
    let HND_AIR_PORT = {lat: 9.925201, lng: 78.119774};
    let SFO_AIR_PORT = {lat: 13.082680, lng: 80.270721};
    let AIR_PORTS = [
      HND_AIR_PORT,
      SFO_AIR_PORT
    ];

    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: AIR_PORTS,
        'tilt': 60
      },
      styles: this.global.mapstyle,
      gestures:{
        rotate:false,
        tilt:false,
        scroll:false,
        zoom:false
      }

    });
    
    if(this.global.from && this.global.to){
      this.route();
      }
/*
    let polyline: Polyline = this.map.addPolylineSync({
      points: AIR_PORTS,
      color: '#AA00FF',
      width: 5,
      geodesic: true,
      clickable: true , // clickable = false in default
      duration: 10000 // = 5 sec.

    });

    polyline.on(GoogleMapsEvent.POLYLINE_CLICK).subscribe((params: any) => {
      let position: LatLng = <LatLng>params[0];

      let marker: Marker = this.map.addMarkerSync({
        position: position,
        title: position.toUrlValue(),
        disableAutoPan: true
      });
      marker.showInfoWindow();
    });
    */
  }

  processForm(event){

  }

  homeitems(){

  }

  addmarker() {
    let POINTS: BaseArrayClass<any> = new BaseArrayClass<any>([
 
      {
        position: {lat:this.global.from.lat, lng:this.global.from.lng},
        iconData: {
          url: "https://mapsplugin.github.io/ionic-googlemaps-quickdemo-v4/assets/imgs/Number-3-icon.png",
          size: {
            width: 24,
            height: 24
          }
        }
      },
      {
        position: {lat:this.global.to.lat, lng:this.global.to.lng},
        iconData: {
          url: "https://mapsplugin.github.io/ionic-googlemaps-quickdemo-v4/assets/imgs/Number-3-icon.png",
          size: {
            width: 24,
            height: 24
          }
        }
      },
  
     
  
    ]);

    let bounds: ILatLng[] = POINTS.map((data: any, idx: number) => {
      console.log(data);
      return data.position;
    });

  
    POINTS.forEach((data: any) => {
      data.disableAutoPan = true;
      let marker: Marker = this.map.addMarkerSync(data);
      
    });

  }


}
