import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmprofilePage } from './mmprofile.page';

describe('MmprofilePage', () => {
  let component: MmprofilePage;
  let fixture: ComponentFixture<MmprofilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmprofilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmprofilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
