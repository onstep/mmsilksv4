import { Component, OnInit,ViewChild } from '@angular/core';
import { ModalController,NavController,Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-things',
  templateUrl: './things.page.html',
  styleUrls: ['./things.page.scss'],
})
export class ThingsPage implements OnInit {
loaded=0;
things:any=[];
category:any=[];
@ViewChild('addfiv')addfiv;
@ViewChild('drop')drop;
fp = 20;
countthings= 0;
co = 20;
// things:any=[
//   {id:1,name:"Kitchen table",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"Refrigerator",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"Toaster",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"Microwave",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"Gas oven  ",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"Blender  ",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"Mixer",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"Dustbin  ",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"Bread bin  ",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"fridge",count:0,image:"assets/washing.svg",category:"steel"},
//   {id:1,name:"Paper towel holder  ",count:0,image:"assets/washing.svg",category:"wood"},
//   {id:1,name:"Couch",count:0,image:"assets/washing.svg",category:"wood"},
//   {id:1,name:"Curtains",count:0,image:"assets/washing.svg",category:"wood"},
//   {id:1,name:"  Coffee table  ",count:0,image:"assets/washing.svg",category:"wood"},
//   {id:1,name:"Lamp",count:0,image:"assets/washing.svg",category:"wood"},
//   {id:1,name:"  Photo frames  ",count:0,image:"assets/washing.svg",category:"wood"},
//   {id:1,name:"  Sofa cushions  ",count:0,image:"assets/washing.svg",category:"wood"},
//   {id:1,name:"fridge",count:0,image:"assets/washing.svg",category:"wood"},


// ];
// category:any=[
//   {id:1,name:"steel"},
//   {id:2,name:"wood"},
 

// ];
  constructor(private navCtrl: NavController, public global: GlobalService , public storage:Storage) { }

  ngOnInit() {
    this.loaddata();
    this.storage.get('things').then((things) => {
       
      if(things){
       // this.global.items=things;
      }
      
  
      });
  }


  add(index,event){
    event.stopPropagation();
    console.log(this.things[index].count);
    console.log(this.things[index].max);
if(this.things[index].count>=this.things[index].max){

return;
}
this.things[index].count++;
//console.log(this.things);
this.global.items=this.things;

  }
  sub(index,event){
    event.stopPropagation();

    console.log(index);
if(this.things[index].count>0){
this.things[index].count--;
this.global.items=this.things;


}
console.log(this.things);
  }


   
  next(){
    this.countthings=0;
    console.log(this.things);
    console.log(this.things.length);
    for (var i = 0; i < this.things.length; i++) {
console.log(this.things[i].count);
      this.countthings= this.countthings+this.things[i].count;
  
    }
    console.log(this.countthings);
    if(this.countthings==0){
this.addfiv.show();
return;

    }
    this.global.items=this.things;
    this.storage.set('things',this.things);

    this.navCtrl.navigateForward('/confirm');

  } 
  loaddata(){

     
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { userid: '1'}

    this.global.http.post(this.global.url+"product/view.php", postData,{observe: 'response'})
      .subscribe(data => {
        console.log(data.body);
        var ddata=data.body;
        // tslint:disable-next-line: triple-equals
        if(ddata['status']=='success'){
//alert('insert otp ');
this.things=ddata['items'];
console.log(this.things);
this.category=ddata['category'];
this.loaded=1;  
setTimeout(() => {
  //this.addfiv.show();
}, 2000);
//this.things=this.global.items;
        }
       }, error => {
        console.log(error);
      });
  }

}
