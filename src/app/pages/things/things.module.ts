import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ThingsPage } from './things.page';
import { FivFeatureDiscoveryModule } from '@fivethree/core';

const routes: Routes = [
  {
    path: '',
    component: ThingsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FivFeatureDiscoveryModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ThingsPage]
})
export class ThingsPageModule {}
