import { Component, OnInit,ViewChild } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { FivFeature } from '@fivethree/core';

@Component({
  selector: 'app-mmsubategory',
  templateUrl: './mmsubategory.page.html',
  styleUrls: ['./mmsubategory.page.scss'],
})
export class MmsubategoryPage implements OnInit {
categoryid:any;
data:any;
loaded:any='0';
subcategory:any;
categoryname:any;
lottieConfig;
private anim: any;
@ViewChild('feature') feature: FivFeature;

  constructor(private activeRoute: ActivatedRoute,public global: GlobalService,private router: Router) { }

  ngOnInit() {
    this.lottieConfig = {
      path: 'assets/front.json',
     renderer: 'canvas',
      autoplay: true,
      loop: false
  };
    this.categoryid = this.activeRoute.snapshot.paramMap.get('id');
    this.categoryname = this.activeRoute.snapshot.paramMap.get('name');
    console.log(this.categoryname);
    this.initial();

  }
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { id: this.categoryid}

    this.global.http.post(this.global.url+"subcategorylisting.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data.body;
       this.subcategory=this.data.subcategory;
   console.log(this.data.subcategory);
      
      

              
  
  
  this.loaded="1"
       }, error => {
       
       })
  }
  gotowish(){
   
      this.router.navigateByUrl('/mmwishlist');
  } 
  gotocart(){
   
      this.router.navigateByUrl('/mmnewcarts');
  }
  gosearch(){
 
    this.router.navigateByUrl('/mmsearch');
  }
}
