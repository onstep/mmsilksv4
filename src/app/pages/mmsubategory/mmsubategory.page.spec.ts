import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmsubategoryPage } from './mmsubategory.page';

describe('MmsubategoryPage', () => {
  let component: MmsubategoryPage;
  let fixture: ComponentFixture<MmsubategoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmsubategoryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmsubategoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
