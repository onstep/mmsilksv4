import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-myorders',
  templateUrl: './myorders.page.html',
  styleUrls: ['./myorders.page.scss'],
})
export class MyordersPage implements OnInit {
orders:any;
ordering:any;
staus:any;
orderimage:any;
  loaded: any='0';
  orderdetail:any;
  cancel:any='0';
  lottieConfig;
  private anim: any;
  constructor(public loadingController: LoadingController,public global: GlobalService,private router: Router,public alertController: AlertController) { }

  ngOnInit() {
    
    this.lottieConfig = {
      path: 'assets/emptys.json',
     renderer: 'canvas',
      autoplay: true,
      loop: false
  };
    this.orders= this.global.customerid;
    console.log( this.global.customerid);
    console.log(this.orders);
   
    this.initial();
  }
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { id: this.orders}

    this.global.http.post(this.global.url+"retrieveallorders.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.orderdetail = data.body;
       console.log(this.orderdetail);
       console.log(this.orderdetail.data);
                 this.staus=this.orderdetail.data;
                 console.log(this.staus);
        this.ordering=this.orderdetail.orders ;
    console.log(this.ordering);
    console.log('hii');
      

              
  
  
    this.loaded="1";
  }, error => {
  
  })
  }
 
 
  async cancels(id) {
    const alert = await this.alertController.create({
      header: 'Are You Sure ',
      message: 'Cancel This Order',
      buttons: [ {
        text: 'Cancel',
        role: 'YES',
        cssClass: 'secondary',
        handler: (blah) => {
         
        }
      }, {
        text: 'YES',
        handler: async () => {

          const loading = await this.loadingController.create({
            message: 'Please Wait a Second',
           
          });
          await loading.present();
          var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { id : id}

    this.global.http.post(this.global.url+"cancelorder.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(async data => {
  
       this.orderdetail = data.body;
         
              this.cancel='1';
            
  
  this.ngOnInit();
    this.loaded="1";
    loading.dismiss();

  }, error => {
  
  })
        }
      }]
    });

    await alert.present();
  }
}
