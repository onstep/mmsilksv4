import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '@components/components.module';

import { IonicModule } from '@ionic/angular';

import { MyordersPage } from './myorders.page';
import { LottieAnimationViewModule } from 'ng-lottie';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
export function playerFactory() {
  return player;
}
const routes: Routes = [
  {
    path: '',
    component: MyordersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LottieAnimationViewModule.forRoot(),
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [MyordersPage]
})
export class MyordersPageModule {}
