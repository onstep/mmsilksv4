import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { style } from '@angular/animations';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class AddressPage implements OnInit {
  useraddress:any={};
  constructor(private storage: Storage,public global: GlobalService,private router: Router,public toastController: ToastController) { }
  
  ngOnInit() {
  }
  async sendnewaddress(){
   
     
    
 if(!this.useraddress.first_name  || !this.useraddress.address_1 || !this.useraddress.city
  || !this.useraddress.postcode || !this.useraddress.state || !this.useraddress.country ){
  //  alert("please fill  all details");
  const toast = await this.toastController.create({
    message: 'please fill  all details',
    position: 'top',
    color:'primary',
    duration: 1000,
  });
  toast.present();
   return;
 }
 this.storage.set('address',this.useraddress);
 this.global.useraddress=this.useraddress;
 this.router.navigateByUrl('/checkout');
 console.log(this.global.useraddress);
  }
}
