import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmwalletPage } from './mmwallet.page';

describe('MmwalletPage', () => {
  let component: MmwalletPage;
  let fixture: ComponentFixture<MmwalletPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmwalletPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmwalletPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
