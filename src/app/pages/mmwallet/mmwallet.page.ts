import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { ActionSheetController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mmwallet',
  templateUrl: './mmwallet.page.html',
  styleUrls: ['./mmwallet.page.scss'],
})
export class MmwalletPage implements OnInit {

  constructor(private router: Router,public alertController: AlertController,public global: GlobalService,public actionSheetController: ActionSheetController) { }
cart:any;
products:any;
loaded:any='0';
orders:any;
wallet:any;
body:any;
role:any;
orderdetail:any;
orderdetails:any;
commision:any;

  ngOnInit() {
    // this.associate();
    this.orders= this.global.customerid;
    this.role= this.global.customerrole;
    console.log(this.global.customerrole);
    console.log(this.orders);

    // this.role= this.global.customerrole;
        console.log(this.role);

        if(this.global.customerid){
          this.order();
        }
    
    if(this.global.customerid){
      this.initial();

    }
 
  }
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

 //   let idarray=[{id:"5182"},{id:"5205"}]
//  console.log(this.global.storecartdetails.all);
// console.log(this.cart);

    // let idarray=this.cart;
    // console.log(idarray);
    let postData =  { id: this.orders }
    console.log(postData);

    this.global.http.post(this.global.url+"wallet.php", postData,{observe: 'response'})      
      //this.http.get(link)
      
       .subscribe(data => {
      
        this.body= data.body;
        this.wallet=this.body.wallet;
     console.log(this.wallet);
      
      

              
  
  
  
       }, error => {
       
       })
  }


  order()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { id: this.orders}

    this.global.http.post(this.global.url+"retrieveallorders.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.orderdetail = data.body;
       this.orderdetails = this.orderdetail.orders;
       this.commision = this.orderdetails.line_items;
     
      console.log(this.commision)

              
  
  
    this.loaded="1";
  }, error => {
  
  })
  }

  async associate() {
    if(this.global.state=='1'){

    const alert = await this.alertController.create({
      // header: 'Are You Want Become a Associate ',
      header: 'Thanks For Your Interst .We Will Contact Soon ',
      // message: 'Cancel This Order',
    
      // buttons: [ {
      //   text: 'CANCEL',
      //   role: 'NO',
      //   cssClass: 'secondary',
      //   handler: (blah) => {
      //   }
      // }, {
      // text: 'YES',
      //     handler: () => {
      //       // this.router.navigateByUrl('/login');

      //     }
      //   }
      // ]
    });

    await alert.present();
  }else{
    // tslint:disable-next-line: no-unused-expression
    this.global.flack='2';
    this.router.navigateByUrl('/login');

}
}
 

 

}
