import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  constructor(public global: GlobalService,private router: Router) { }
  category:any;
  data:any;
  catg:any;
  products:any;
  banner:any;
  featured:any;
  totalproducts:any;
  offer:any;
  cate:any;
  subcat:any;
  loaded:any='0';
  titleMode = 'hide';
  position = 'center';
  ngOnInit() {
    this.initial();
  }
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { userid: '1'}

    this.global.http.post(this.global.url+"index.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data.body;
       this.products=this.data.categories;
       this.totalproducts=this.data.subcat;

       this.banner=this.data.banner;
       this.featured=this.data.featuredproduct;
        this.offer=this.data.offer;
       console.log(this.featured);
      
       console.log(this.global.storecartdetails.count);
 
              
  
  
  this.loaded="1"
       }, error => {
       
       })
  }
  gotowish(){
   
    this.router.navigateByUrl('/mmwishlist');
} 
gotocart(){
 
    this.router.navigateByUrl('/mmnewcarts');
}
gosearch(){
 
  this.router.navigateByUrl('/mmsearch');
}

}
