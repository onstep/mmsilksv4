import { AlertController } from '@ionic/angular';
import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { FivDialog } from '@fivethree/core';
import { ModalController,NavController,Platform } from '@ionic/angular';
import { GlobalService } from '../../services/global.service';
import { Storage } from '@ionic/storage';
import { ThanosSnapService } from '@wellwind/ngx-thanos';

@Component({
  selector: 'app-confirmtruck',
  templateUrl: './confirmtruck.page.html',
  styleUrls: ['./confirmtruck.page.scss'],
})
export class ConfirmtruckPage implements OnInit {
  km:any;
  price:any;
  loaded:any=0;
  formdata:any={};
truckid;
truckdata:any={};
public lottieConfig: Object;
snap = false;
canRewind = false;
@ViewChild('dialog') dialog: FivDialog;
backdrop = true;
pull = true;
verticalAlign = 'bottom';
horizontalAlign = 'left';
shape = 'card';
duration = 0;
inDuration = '220';
outDuration = '180';
estimated=0;
sharing:any=false;

count=0;
hidden=false;
shareloading=false;
coupondata={};
couponapplied=false;
buttondis=false;
firstprice;
firstsharingprice;

    private anim: any;
    private animationSpeed: number = 1;

trucks=[
  {
"id": 1,
"image":"assets/isotruck/isometric truck-01.png",
"name": "TATA Ace"
},   {
"id": 1,
"image":"assets/isotruck/isometric truck-02.svg",
"name": "ATA ECHIER"
},   {
"id": 1,
"image":"assets/isotruck/isometric truck-03.svg",
"name": "AOSHK LEYLAND"
},   {
"id": 1,
"image":"assets/isotruck/isometric truck-04.svg",
"name": "TATA SMALL"
},   {
"id": 1,
"image":"assets/isotruck/isometric truck-05.svg",
"name": "TATA COVERED"
},   {
"id": 1,
"image":"assets/isotruck/isometric truck-06.svg",
"name": "ECGIER"
}
];
  constructor(private router: NavController,public alertController: AlertController,private storage: Storage,private navCtrl: NavController, public global: GlobalService) {}
promospin:any=0;
  ngOnInit() {
  this.lottieConfig = {
            path: 'assets/truckld.json',
           renderer: 'canvas',
            autoplay: true,
            loop: true
        };

  // this.lottieConfig= {
  //         path: '/assets/animation.json'
  //       };

    this.storage.get('km').then((km) => {
       
      if(km){
        this.km=km;
        console.log(this.km);
        this.storage.get('selectedtruck').then((truckid) => {
       
          if(truckid){
            this.truckid=truckid;

            this.truckdata=this.trucks[truckid-1]

            console.log(this.truckid+'trcukid');
        this.nextt();
          }
     
          });

      }
 
      });
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Are you sure to book!',
      message: '* Warning content one.<br>* Warning content one <br>* Warning content one ',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'dark',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Book Now',
          cssClass: 'dark',
          handler: () => {
            this.booknow();
           // this.router.navigateForward('/success');
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
next(){
  this.router.navigateForward('/success');

}
  nextt(){
   // alert(this.sharing);
   
this.formdata.userid="1";
this.formdata.truckid=this.truckid;
this.formdata.pickupdate=new Date();
console.log(this.formdata.pickupdate);
this.formdata.distance=this.km.value;
this.formdata.fromaddress=this.global.from;
this.formdata.toaddress=this.global.to;
// this.formdata.form=this.global.mappageform;
console.log(this.formdata);

    // {userid:1,distance:500,city:a,
    //   fromaddress:{lat:19.34,lng:23.99,address:"Kothaval chanvaid street",lift:true,floor:2},
    //   toaddress:{lat:19.34,lng:23.99,address:"ramnad",lift:false,floor:3},
    //   things: [{id:1,name:"Kitchen table",count:5,image:"assets/washing.svg",categoryid:"2"},
    //           {id:1,name:"Refrigerator",count:0,image:"assets/washing.svg",categoryid:"2"},
    //           {id:1,name:"Toaster",count:4,image:"assets/washing.svg",categoryid:"2"},
    //           {id:1,name:"Microwave",count:9,image:"assets/washing.svg",categoryid:"2"}],
    //  sharing:true}
 //   this.navCtrl.navigateForward('/confirm')

 var headers = new Headers();
 headers.append('Accept', 'application/json');
 headers.append('Content-Type', 'application/json' );


 let postData =  this.formdata;

 this.global.http.post(this.global.url+"estimation/newtruck.php", postData,{observe: 'response'})
   .subscribe(data => {
     console.log(data.body);
     // tslint:disable-next-line: triple-equals
     if(data.body['status']=='sucess'){
      // alert(data.body['price']);
       this.price=data.body['price'];
       this.formdata.id=data.body['orderid'];
      this.loaded=1;

//alert('insert otp ');;
     }
    }, error => {
     console.log(error);
   });
   

  }

  estimate2(){
    // alert(this.sharing);
    this.shareloading=true;
    this.hidden=true;
     this.storage.set('sharing',this.sharing);
 this.formdata.userid="1";
 this.formdata.distance=this.km.value;
 this.formdata.fromaddress=this.global.from;
 this.formdata.toaddress=this.global.to;
 this.formdata.sharing=this.sharing;
 this.formdata.things=this.global.items;
 this.formdata.form=this.global.mappageform;
 console.log(this.formdata);
//  this.navCtrl.navigateForward('/confirm')
 
     // {userid:1,distance:500,city:a,
     //   fromaddress:{lat:19.34,lng:23.99,address:"Kothaval chanvaid street",lift:true,floor:2},
     //   toaddress:{lat:19.34,lng:23.99,address:"ramnad",lift:false,floor:3},
     //   things: [{id:1,name:"Kitchen table",count:5,image:"assets/washing.svg",categoryid:"2"},
     //           {id:1,name:"Refrigerator",count:0,image:"assets/washing.svg",categoryid:"2"},
     //           {id:1,name:"Toaster",count:4,image:"assets/washing.svg",categoryid:"2"},
     //           {id:1,name:"Microwave",count:9,image:"assets/washing.svg",categoryid:"2"}],
     //  sharing:true}
  // this.navCtrl.navigateForward('/confirm')
 
  var headers = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json' );
 
 
  let postData =  this.formdata;
 
  this.global.http.post(this.global.url+"estimation/new.php", postData,{observe: 'response'})
    .subscribe(data => {
      console.log(data.body);
      // tslint:disable-next-line: triple-equals
      if(data.body['status']=='success'){
 this.estimated=1;
 //this.hidden=true;
 var timer = setTimeout(() => {
  this.price=data.body['price'];
 // alert();
 this.hidden=false;
 this.shareloading=false;
 console.log('jjj');
 console.log(this.sharing);
 // console.log(this.price);
 
}, 2500);
 this.loaded=1;

//this.animateValue( 0, data.body['price'], 200);
 //alert(this.price);
      }
     }, error => {
      console.log(error);
    });
    
 
   }

   applyoffer(){
    this.hidden=true;
    this.buttondis=true;
    // alert(this.sharing);
    if(!this.formdata.coupon){
alert("Please enter the coupon");
this.hidden=false;
this.buttondis=false;

return;
    }
    this.promospin=true;
     this.storage.set('sharing',this.sharing);
 this.formdata.userid="1";
 this.formdata.price=this.price;
//  this.formdata.distance=this.km.value;
//  this.formdata.fromaddress=this.global.from;
//  this.formdata.toaddress=this.global.to;
//  this.formdata.sharing=this.sharing;
//  this.formdata.things=this.global.items;
//  this.formdata.form=this.global.mappageform;
 
 console.log(this.formdata);

 
  var headers = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json' );
 
 
  let postData =  {formdata:this.formdata};
 
  this.global.http.post(this.global.url+"estimation/promo.php", postData,{observe: 'response'})
    .subscribe(data => {
      console.log(data.body);
      // tslint:disable-next-line: triple-equals
      if(data.body['status']=='success'){
        if(data.body['applied']=='yes'){
                  this.coupondata=data.body;
                  this.couponapplied=true;
                  this.promospin=false;
                  var timer = setTimeout(() => {
                    this.price=data.body['value'];
                   // alert();
                   this.hidden=false;
                   this.buttondis=false;

                   // console.log(this.price);
                   
                  }, 2500);

        }else{
          alert(data.body['message']);
          this.promospin=false;
          var timer3 = setTimeout(() => {
           // alert();
           this.hidden=false;
           this.buttondis=false;

           // console.log(this.price);
           
          }, 2500);
        }

      }
     }, error => {
      console.log(error);
    });
    
 
   }


   booknow(){
    this.buttondis=true;
  
     this.storage.set('sharing',this.sharing);
 this.formdata.userid="1";
 this.formdata.price=this.price;
//  this.formdata.distance=this.km.value;
//  this.formdata.fromaddress=this.global.from;
//  this.formdata.toaddress=this.global.to;
 this.formdata.sharing=0;
//  this.formdata.things=this.global.items;
//  this.formdata.form=this.global.mappageform;
 
 console.log(this.formdata);

 
  var headers = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json' );
 
 
  let postData =  this.formdata;
 
  this.global.http.post(this.global.url+"estimation/confirmbooking.php", postData,{observe: 'response'})
    .subscribe(data => {
      console.log(data.body);
      // tslint:disable-next-line: triple-equals
      if(data.body['status']=='success'){
   this.router.navigateRoot('/success/'+data.body['orderid']);

      }
     }, error => {
      console.log(error);
    });
    
 
   }


}
