import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LottieAnimationViewModule } from 'ng-lottie';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
export function playerFactory() {
  return player;
}
import { IonicModule } from '@ionic/angular';

import { ConfirmtruckPage } from './confirmtruck.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmtruckPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
   
        LottieAnimationViewModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [ConfirmtruckPage]
})
export class ConfirmtruckPageModule {}
