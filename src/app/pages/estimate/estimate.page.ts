import { Component, OnInit,ViewChild } from '@angular/core';
import { FivDialog } from '@fivethree/core';
import { ModalController,NavController,Platform } from '@ionic/angular';
import { GlobalService } from '../../services/global.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-estimate',
  templateUrl: './estimate.page.html',
  styleUrls: ['./estimate.page.scss'],
})
export class EstimatePage implements OnInit {
  @ViewChild('dialog') dialog: FivDialog;
  backdrop = true;
  pull = true;
  verticalAlign = 'bottom';
  horizontalAlign = 'left';
  shape = 'card';
  duration = 0;
  inDuration = '220';
  outDuration = '180';
sharing:any=false;
km;
formdata:any={};
  constructor(private storage: Storage,private navCtrl: NavController, public global: GlobalService) { }

  ngOnInit() {
    console.log(this.global);
    this.storage.get('sharing').then((val) => {
      console.log('val');
      console.log(val);
    if(val){
      this.sharing=val;
    }
   
      this.storage.get('km').then((km) => {
       
      if(km){
        this.km=km;
      }
      
      this.storage.get('things').then((things) => {
       
        if(things){
          this.global.items=things;
        }
        
    
        });
      });
    });
  
  }

//   loaddata(){
     
//     var headers = new Headers();
//     headers.append('Accept', 'application/json');
//     headers.append('Content-Type', 'application/json' );


//     let postData =  { userid: '1'}

//     this.global.http.post(this.global.url+"product/view.php", postData,{observe: 'response'})
//       .subscribe(data => {
//         console.log(data.body);
//         var ddata=data.body;
//         // tslint:disable-next-line: triple-equals
//         if(ddata['status']=='success'){
// //alert('insert otp ');
// this.things=ddata['items'];
// console.log(this.things);
// this.category=ddata['category'];
// this.loaded=1;
// //this.things=this.global.items;
//         }
//        }, error => {
//         console.log(error);
//       });
//   }


  next(){
   // alert(this.sharing);
   
    this.storage.set('sharing',this.sharing);
this.formdata.userid="1";
this.formdata.distance=this.km.value;
this.formdata.fromaddress=this.global.from;
this.formdata.toaddress=this.global.to;
this.formdata.sharing=this.sharing;
this.formdata.things=this.global.items;
this.formdata.form=this.global.mappageform;
console.log(this.formdata);
this.navCtrl.navigateForward('/confirm')

    // {userid:1,distance:500,city:a,
    //   fromaddress:{lat:19.34,lng:23.99,address:"Kothaval chanvaid street",lift:true,floor:2},
    //   toaddress:{lat:19.34,lng:23.99,address:"ramnad",lift:false,floor:3},
    //   things: [{id:1,name:"Kitchen table",count:5,image:"assets/washing.svg",categoryid:"2"},
    //           {id:1,name:"Refrigerator",count:0,image:"assets/washing.svg",categoryid:"2"},
    //           {id:1,name:"Toaster",count:4,image:"assets/washing.svg",categoryid:"2"},
    //           {id:1,name:"Microwave",count:9,image:"assets/washing.svg",categoryid:"2"}],
    //  sharing:true}
 this.navCtrl.navigateForward('/confirm')

 var headers = new Headers();
 headers.append('Accept', 'application/json');
 headers.append('Content-Type', 'application/json' );


 let postData =  this.formdata;

 this.global.http.post(this.global.url+"estimation/new.php", postData,{observe: 'response'})
   .subscribe(data => {
     console.log(data.body);
     // tslint:disable-next-line: triple-equals
     if(data.body['status']=='success'){

//alert('insert otp ');;
     }
    }, error => {
     console.log(error);
   });
   

  }
  close() {}

  over() {}
}
