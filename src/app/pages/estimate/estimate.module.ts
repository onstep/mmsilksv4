import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '@components/components.module';

import { IonicModule } from '@ionic/angular';

import { EstimatePage } from './estimate.page';

import { FivDialogModule } from '@fivethree/core';
const routes: Routes = [
  {
    path: '',
    component: EstimatePage
  }
];

@NgModule({
  imports: [
    ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    FivDialogModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EstimatePage]
})
export class EstimatePageModule {}
