import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MmreferfriendPage } from './mmreferfriend.page';


const routes: Routes = [
  {
    path: '',
    component: MmreferfriendPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [MmreferfriendPage]
})
export class MmreferfriendPageModule {}
