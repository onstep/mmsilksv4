import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmreferfriendPage } from './mmreferfriend.page';

describe('MmreferfriendPage', () => {
  let component: MmreferfriendPage;
  let fixture: ComponentFixture<MmreferfriendPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmreferfriendPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmreferfriendPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
