import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmsearchPage } from './mmsearch.page';

describe('MmsearchPage', () => {
  let component: MmsearchPage;
  let fixture: ComponentFixture<MmsearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmsearchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmsearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
