import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import {  Input, ViewChild,ElementRef,Renderer, AfterViewChecked,ChangeDetectorRef  } from '@angular/core';


@Component({
  selector: 'app-mmsearch',
  templateUrl: './mmsearch.page.html',
  styleUrls: ['./mmsearch.page.scss'],
})
export class MmsearchPage implements OnInit {
  @ViewChild('myinput') myinput;

  constructor(private _changeDetectionRef: ChangeDetectorRef,public global: GlobalService,private router: Router) { }
loaded:any;
id:any;
cart:any;
item:any;
searchresult:any;
searching:any;
output:any;
lottieConfig;
event:any;
  private anim: any;
  ngOnInit() {
    this.lottieConfig = {
      path: 'assets/empty.json',
     renderer: 'canvas',
      autoplay: true,
      loop: false
  };
  console.log(this.event);
  setTimeout(() => 
  {
  //  this.renderer.invokeElementMethod(this.otp1.nativeElement, 'focus');
    this.myinput.setFocus();
    this._changeDetectionRef.detectChanges();
  }
  , 800);
  }
  search()
  { 
    if(this.event.length>2){
return;
    }
  this.searching=null;
    console.log(this.event);
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

 console.log(this.global.storecartdetails.all);
    // let idarray=this.cart;
    let postData =  { search :this.event }
    console.log(postData);

    this.global.http.post(this.global.url+"search.php", postData,{observe: 'response'})      
      //this.http.get(link)
      
       .subscribe(data => {
  
        this.searchresult= data.body;
        this.searching= this.searchresult.data;
        this.output= this.searchresult.status;
       console.log(this.searching);
       //this.event=null;
      
      

      //  console.log(this.global.storecartdetails.count);
              
  
  
  this.loaded="1";
       }, error => {
       
       })
  
}
  
  searchdetail(id){
   
    this.router.navigateByUrl('/mmproductdetails/'+id);
  }
}
