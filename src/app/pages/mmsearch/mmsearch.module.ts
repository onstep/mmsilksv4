import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MmsearchPage } from './mmsearch.page';
import { ComponentsModule } from '@components/components.module';
import { LottieAnimationViewModule } from 'ng-lottie';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
export function playerFactory() {
  return player;
}
const routes: Routes = [
  {
    path: '',
    component: MmsearchPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    LottieAnimationViewModule.forRoot(),
    ComponentsModule

  ],
  declarations: [MmsearchPage]
})
export class MmsearchPageModule {}
