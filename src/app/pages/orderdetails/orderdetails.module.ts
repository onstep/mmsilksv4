import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FivCenterModule } from './../../../../projects/core/src/lib/center/center.module';
import { FivRippleModule } from './../../../../projects/core/src/lib/ripple/ripple.module';
import { FivExpandableModule } from '@fivethree/core';
import { FivAppBarModule } from '@fivethree/core';

import { IonicModule } from '@ionic/angular';

import { OrderdetailsPage } from './orderdetails.page';

const routes: Routes = [
  {
    path: '',
    component: OrderdetailsPage
  }
];

@NgModule({
  imports: [
    FivExpandableModule,
    FivRippleModule,
    FivCenterModule,
    CommonModule,
    FormsModule,
    IonicModule,
    FivAppBarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderdetailsPage]
})
export class OrderdetailsPageModule {}
