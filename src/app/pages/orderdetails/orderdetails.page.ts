import { FivStepper } from '@fivethree/core';
import { AlertController } from '@ionic/angular';
import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { FivDialog } from '@fivethree/core';
import { ModalController,NavController,Platform } from '@ionic/angular';
import { GlobalService } from '../../services/global.service';
import { Storage } from '@ionic/storage';
import { ThanosSnapService } from '@wellwind/ngx-thanos';
import {ActivatedRoute} from '@angular/router';
declare var RazorpayCheckout: any;
@Component({
  selector: 'app-orderdetails',
  templateUrl: './orderdetails.page.html',
  styleUrls: ['./orderdetails.page.scss'],
})
export class OrderdetailsPage implements OnInit {
  loaded:any=false;
  formdata:any={};
  data:any;
  orderid:any;
  titleLayout = 'show';
  position = 'center';
  fabVisible = false;
  icon = 'logo-whatsapp';

  constructor(private activeRoute: ActivatedRoute, router: NavController,public alertController: AlertController,private storage: Storage,private navCtrl: NavController, public global: GlobalService) {}
  
    ngOnInit() {
      this.orderid = +this.activeRoute.snapshot.paramMap.get('orderid');
      this.getdata();
    }
  
    ionViewDidEnter() {
      // this.stepperV.select(0);
    }
  all(){
    alert('Payment success');
  }
  al() {
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: 'INR', // your 3 letter currency code
      key: 'rzp_live_lRlxGc3fOj3GdP', // your Key Id from Razorpay dashboard
      amount: 1*100, // Payment amount in smallest denomiation e.g. cents for USD
      name: 'foo',
      prefill: {
        email: 'admin@enappd.com',
        contact: '9621323231',
        name: 'Enappd'
      },
      theme: {
        color: '#F37254'
      },
      modal: {
        ondismiss: function () {
          alert('dismissed')
        }
      }
    };

    var successCallback = function (payment_id) {
      alert('payment_id: ' + payment_id);
    };

    var cancelCallback = function (error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }
    getdata(){
    
  
   this.formdata.userid="1";
   this.formdata.id=this.orderid;
  //  this.formdata.distance=this.km.value;
  //  this.formdata.fromaddress=this.global.from;
  //  this.formdata.toaddress=this.global.to;
  //  this.formdata.sharing=this.sharing;
  //  this.formdata.things=this.global.items;
  //  this.formdata.form=this.global.mappageform;
   
   console.log(this.formdata);
  
   
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );
   
   
    let postData =  this.formdata;
   
    this.global.http.post(this.global.url+"estimation/viewsingle.php", postData,{observe: 'response'})
      .subscribe(data => {
        console.log(data.body);
        // tslint:disable-next-line: triple-equals
        if(data.body['status']=='success'){
          this.data=data.body['records'][0];
       this.loaded=true;
  
        }
       }, error => {
        console.log(error);
      });
      
   
     }
  
  
  }
  