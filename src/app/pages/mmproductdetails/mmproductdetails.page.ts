import { Component, OnInit, ViewChild } from '@angular/core';
import { FivDialog } from '@fivethree/core';
//import { Select } from 'ionic-angular';
import { Platform, Events, IonSelect } from '@ionic/angular';
import { GlobalService } from '../../services/global.service';
import {ActivatedRoute} from '@angular/router';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
 import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-mmproductdetails',
  templateUrl: './mmproductdetails.page.html',
  styleUrls: ['./mmproductdetails.page.scss'],
})
export class MmproductdetailsPage implements OnInit {
  @ViewChild('dialog') dialog: FivDialog;
  // @ViewChild(Select) select: Select;
  @ViewChild('count') countselect: IonSelect;
  showcountselect = true;

  backdrop = false;
  pull = true;
  verticalAlign = 'bottom';
  horizontalAlign = 'middle';
  shape = 'fill';
  duration = 0;
  inDuration = '220';
  outDuration = '180';
  loaded: any='0';
  data: any;
  categoryid: any;
  productdetail:any;
  count:any;
  cartbuttonloading:any='1';
  preorderstatus:any;
  preordername:any;
  related: any;
  wishlisted:any='0';
  constructor(public social:SocialSharing,  public toastController: ToastController ,private storage: Storage,public global: GlobalService,private activeRoute: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    this.categoryid = this.activeRoute.snapshot.paramMap.get('id');
console.log(this.categoryid);
this.storage.get('storecart').then((vall) => {
  //  console.log(vall);
    var  index = vall.findIndex((obj => obj.productid == this.categoryid));
    this.count=vall[index].count;});
    
    this.initial();
    this.iswishlist();

  }
  preorder(){
  var i;
  for(i=0; i<this.productdetail.attributes.length; i++ ){
    this.preorderstatus=this.productdetail.attributes[i].options;
    
  }    

  }

  share(){
    console.log('share');
    this.social.share(this.productdetail.name+'_'+this.productdetail.short_description, this.productdetail.name,  this.productdetail.images[0].src).then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
      console.log('error');

    });
    

  }
  preordering() {
  console.log(this.preordername);
  this.preordername="";
    }

  close() {
  this.countselect.open();
  }

  over() {}
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );
  

    let postData =  { id: this.categoryid}

    this.global.http.post(this.global.url+"singleproduct.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data.body;
       this.productdetail=this.data.product;
       this.related=this.data.related;
       console.log(this.related);
     console.log(this.productdetail.stock_status);
    this.preorder();
      
      
    
              
  
    this.productcount();

  this.loaded="1";
       }, error => {
       
       })
  }
  
  productcount(){

    this.storage.get('storecart').then((val) => {
  //console.log(val);
  var  index = val.findIndex((obj => obj.productid == this.productdetail.id));
  
   if(index == -1){
    this.count=0;
  
  }
  else{
  this.count=val[index].count;
  console.log(this.count);
  
  }
  });
   }
   

   async add(){
    this.addtostorecart(this.productdetail.id,this.productdetail.price,this.productdetail.regular_price);
    
      const toast = await this.toastController.create({
        message: 'Your Product is Added To Cart',
        duration: 2000,
        position: 'top',
        color:'myred'
      });
      toast.present();
    
  }
  addtostorecart(productid,price,mrp){
    this.count=1;
    this.cartbuttonloading='1';
    this.storage.get('storecart').then((val) => {

    var  index = val.findIndex((obj => obj.productid == productid));
    //alert(index);
    if(index== -1){
   let item ={productid: productid, price: price,mrp:mrp,count:1};
  

  val.push(item),
this.storage.set('storecart',val).then((val) => {
  var count;
  this.storage.get('storecart').then((vall) => {
  //  console.log(vall);
    var  index = vall.findIndex((obj => obj.productid == productid));
  
     if(index == -1){
     count=0;
    
    }
    else{
   count=vall[index].count;
   this.productcount();
   this.cartbuttonloading='0';

    }
  
 this.global= count;
 console.log(this.global);

    });
this.global.storecart();

});
  
this.global.storecart();

  var  index = val.findIndex((obj => obj.productid == productid));
  //val[index].count++;
  //this.storage.set('storecart',val);
// alert(val[index].count);



    }else{
      val[index].count++;
      this.storage.set('storecart',val).then((val) => {
        var count;
        this.storage.get('storecart').then((vall) => {
       //   console.log(vall);
          var  index = vall.findIndex((obj => obj.productid == productid));
        
           if(index == -1){
           count=0;
          
          }
          else{
         count=vall[index].count;
    
          }
        
       this.count= count;
       console.log(this.count);
    
          });
        this.global.storecart();
        
        });
    }
    });
   this.global.storecart();
  
   


console.log(this.global);
  }

   reduceinstorecart(productid){
    var count; 
       this.storage.get('storecart').then((val) => {
   
       var  index = val.findIndex((obj => obj.productid == productid));
       //alert(index);
      
         val[index].count--;
         if(val[index].count==0){
           val.splice(index,1);
         }
         this.storage.set('storecart',val).then((val) => {
   
   
          this.global.storecart();

   
   
           this.storage.get('storecart').then((val) => {
          //   console.log(val);
             var  index = val.findIndex((obj => obj.productid == productid));
           
              if(index == -1){
              count=0;
              this.global.storecart();

             }
             else{
            count=val[index].count;
   
             }
           
          this.count= count;
          console.log(this.count);
          this.global.storecart();

             });
   
   
   
   
   
   
           
   
   
   
   
           });
       });
       this.global.storecart();
 
     }

     gotowish(){
   
      this.router.navigateByUrl('/mmwishlist');
  } 
  gotocart(){
   
      this.router.navigateByUrl('/mmnewcarts');
  }
  gosearch(){
 
    this.router.navigateByUrl('/mmsearch');
  }

  iswishlist(){
    ///retrive the wishlist data from localstorage
  this.storage.get('wishlist').then((val) => {
//console.log(val);
var  index = val.findIndex((obj => obj.productid == this.categoryid));
//if already in list 
if(index == -1){
  this.wishlisted='0';

}
else{
  this.wishlisted='1';
  console.log(this.wishlisted);


}
});
 }

  wishlist(){
    // event.stopPropagation();

  let productid=this.categoryid;
   //retrive the wishlist data from localstorage 
    this.storage.get('wishlist').then((val) => {

      console.log(val);
   //it will return -1 if product not addeded in wishlist else return the index of the product

    var  index = val.findIndex((obj => obj.productid == productid));

    if(index== -1){
      //declare the array with product id
   let item ={productid: productid};
  
//push the declared data to the array from localstorage
  val.push(item);
  ///update the new array to localstorage
this.storage.set('wishlist',val).then((val) => {
  this.iswishlist();

  ///set the value in localstorage
  this.storage.get('wishlist').then((val) => {
    console.log(val);

  });
});
 
    }
    else{
     // delete val[index];
      console.log(val);
      //if already added remove  element the array
      val.splice(index, 1);

      ///update the new array to localstorage
    this.storage.set('wishlist',val).then((val) => {
      this.iswishlist();

      ///set the value in localstorage
      this.storage.get('wishlist').then((val) => {
        console.log(val);
    
      });
    });
    }
  });
 
   
}
  
}
