import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FivIconModule } from '@fivethree/core';
import { IonicModule } from '@ionic/angular';
// import { SocialSharing } from '@ionic-native/social-sharing';

import { MmproductdetailsPage } from './mmproductdetails.page';
import { ComponentsModule } from '@components/components.module';
import { FivDialogModule } from '@fivethree/core';
const routes: Routes = [
  {
    path: '',
    component: MmproductdetailsPage
  }
];

@NgModule({
  imports: [
    FivIconModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    // SocialSharing,
        FivDialogModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MmproductdetailsPage]
})
export class MmproductdetailsPageModule {}
