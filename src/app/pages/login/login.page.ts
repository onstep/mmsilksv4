import { Component, OnInit, } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  step:any='1';
  ddata:any={};
  otp:any={};
  mobile:any;
  spinner:any;
  
  statusBar: any;
  
  constructor(public toastController: ToastController,public loadingController: LoadingController, private fb: FormBuilder,private storage: Storage, public global: GlobalService,private router: Router) {
    // global.otp();
       }
       ngOnInit() {
        console.log( this.global.customerid);
        
     }
     otpController(event,next,prev){
      if(event.target.value.length < 1 && prev){
        prev.setFocus()
      }
      else if(next && event.target.value.length>0){
        next.setFocus();
      }
      else {
       return 0;
      } 
   }

   async otpverify()
   {console.log(this.otp.one);

    // const loading = await this.loadingController.create({
    //   message: 'Please Wait a Second',
     
    // });
    // await loading.present();
    this.spinner='0';

     var otp=this.otp.one+this.otp.two+this.otp.three+this.otp.four;
    let postData =  { mobile: this.mobile,otp:otp}
    // let postData =  { mobile: '9025433139','1234'}
      
  
    this.global.http.post(this.global.url+"otpcheck.php", postData,{observe: 'response'})
      .subscribe(async data => {
        console.log(data.body);
         this.ddata =data.body
        
        

        // tslint:disable-next-line: triple-equals
        if(this.ddata.status=='success'){

          this.storage.set('userid',this.ddata.customerid );
          this.storage.set('role',this.ddata.role );
          this.global.customerid=this.ddata.customerid;
          console.log( this.global.customerid);
          this.global.customerrole=this.ddata.role;
          console.log( this.global.customerrole);

          this.storage.get('userid');
          console.log(this.storage.get('userid'));

          this.global.state='1';
this.router.navigateByUrl('myhome/tabs/tab1');
if(this.global.flack =='1'){
  this.router.navigateByUrl('/address');

}
if(this.global.flack=='2'){
  this.router.navigateByUrl('/mmwallet');

}

console.log(this.global.state);
// this.step='2';
console.log(this.step);
this.spinner='1';

        }else{
          const toast = await this.toastController.create({
            message: ' Incorrect OTP ',
            duration: 2000,
            color:'myred'
          });
          toast.present();
        }
        // loading.dismiss();

       }, async error => {
       
      });
  }
     
   
   
    login(data){
  
      var headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Content-Type', 'application/json' );
  console.log(data.mobile);
  this.mobile=data.mobile;
      let postData =  { mobile: data.mobile}
      
  
      this.global.http.post(this.global.url+"otp.php", postData,{observe: 'response'})
        .subscribe(data => {
          console.log(data.body);
           this.ddata =data.body
          // tslint:disable-next-line: triple-equals
          if(this.ddata.status=='success'){
  //alert('insert otp ');
  this.step='2';
  console.log(this.step);
          }
         }, error => {
          console.log(error);
        });
    }
  
  }