import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmproductlistingPage } from './mmproductlisting.page';

describe('MmproductlistingPage', () => {
  let component: MmproductlistingPage;
  let fixture: ComponentFixture<MmproductlistingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmproductlistingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmproductlistingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
