import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FivIconModule } from '@fivethree/core';

import { IonicModule } from '@ionic/angular';

import { MmproductlistingPage } from './mmproductlisting.page';
import { ComponentsModule } from '@components/components.module';
import { LottieAnimationViewModule } from 'ng-lottie';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
export function playerFactory() {
  return player;
}

const routes: Routes = [
  {
    path: '',
    component: MmproductlistingPage
  }
];

@NgModule({
  imports: [
    FivIconModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
   LottieAnimationViewModule.forRoot(),
    ComponentsModule
  ],
  declarations: [MmproductlistingPage]
})
export class MmproductlistingPageModule {}
