import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mmproductlisting',
  templateUrl: './mmproductlisting.page.html',
  styleUrls: ['./mmproductlisting.page.scss'],
})
export class MmproductlistingPage implements OnInit {
  data: any;
  status: any;
  productlist: string;
  loaded: any='0';
  categoryid: any;
  categoryname:any;
  lottieConfig;
  private anim: any;
  constructor(private activeRoute: ActivatedRoute,public global: GlobalService,private router: Router) { }

  ngOnInit() {
    this.lottieConfig = {
      path: 'assets/empty.json',
     renderer: 'canvas',
      autoplay: true,
      loop: false
  };
    this.categoryid = this.activeRoute.snapshot.paramMap.get('id');
    this.categoryname = this.activeRoute.snapshot.paramMap.get('name');
    console.log(this.categoryid);
    this.initial();
  }
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { id: this.categoryid}

    this.global.http.post(this.global.url+"productlisting.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data.body;
       this.status=this.data.status;
       this.productlist=this.data.products;
     console.log( this.productlist);
      
      

              
  
  
  this.loaded="1"
       }, error => {
       
       })
  }



  low()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { id: this.categoryid}

    this.global.http.post(this.global.url+"sortbypriceasc.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data.body;
       this.productlist=this.data.products;
     console.log( this.productlist);
      
      

              
  
  
  this.loaded="1"
       }, error => {
       
       })
  }
  high()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { id: this.categoryid}

    this.global.http.post(this.global.url+"sortbypricedesc.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data.body;
       this.productlist=this.data.products;
     console.log( this.productlist);
      
      

              
  
  
  this.loaded="1"
       }, error => {
       
       })
  }
  latest()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { id: this.categoryid}

    this.global.http.post(this.global.url+"sortbydate.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data.body;
       this.productlist=this.data.products;
     console.log( this.productlist);
      
      

              
  
  
  this.loaded="1"
       }, error => {
       
       })
  }
  gotowish(){
   
    this.router.navigateByUrl('/mmwishlist');
} 
gotocart(){
 
    this.router.navigateByUrl('/mmnewcarts');
}
gosearch(){
 
  this.router.navigateByUrl('/mmsearch');
}
}
