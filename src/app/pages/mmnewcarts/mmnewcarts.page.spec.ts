import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmnewcartsPage } from './mmnewcarts.page';

describe('MmnewcartsPage', () => {
  let component: MmnewcartsPage;
  let fixture: ComponentFixture<MmnewcartsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmnewcartsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmnewcartsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
