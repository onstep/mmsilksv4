import { Component, OnInit,ViewChild } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
import { FivFeature } from '@fivethree/core';

@Component({
  selector: 'app-mmnewcarts',
  templateUrl: './mmnewcarts.page.html',
  styleUrls: ['./mmnewcarts.page.scss'],
})
export class MmnewcartsPage implements OnInit {
  wishid:any;
  productdetail: any;
  data: any;
  loaded:any='0';
  cart:any;
  lottieConfig;
  id:any;
  item:any;
  cartbut:any;
  rme:any;
 
@ViewChild('feature') feature: FivFeature;
 
  private anim: any;
  constructor(public toastController: ToastController,private activeRoute: ActivatedRoute,private storage: Storage,public global: GlobalService,private router: Router) { }

  ngOnInit() {
  
    // this.global.cart='0';
  
    this.lottieConfig = {
      path: 'assets/emptys.json',
     renderer: 'canvas',
      autoplay: true,
      loop: false
  };
    
   this.global.storage.get('storecart').then((val) => {
    console.log(val);
    this.cart=val;
  //alert(val.length);
var total:number=0;
var totalmrp:number=0;
var totalcount:number=0;

for(var i=0;i<val.length; i++){
      total=total + (val[i].price * val[i].count);
      totalcount=totalcount+val[i].count;
      console.log(totalcount);
       }
for(var i=0;i<val.length; i++){
      totalmrp=totalmrp + (val[i].mrp * val[i].count);
      totalcount=totalcount+val[i].count;
      console.log(totalcount);
       }
       this.initial();

   // alert("price"+this.storecartdetails.price);
    //alert("price"+this.storecartdetails.count);
    //alert("price"+this.storecartdetails.totalcount);
  });
 
     
      
  }
  ionViewDidEnter(){
    // this.initial();

}
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

 //   let idarray=[{id:"5182"},{id:"5205"}]
 console.log(this.global.storecartdetails.all);
    let idarray=this.cart;
    let postData =  { id:idarray }
    console.log(postData);

    this.global.http.post(this.global.url+"cart.php", postData,{observe: 'response'})      
      //this.http.get(link)
      
       .subscribe(data => {
  
        this.global.carting= data.body;
        this.id=this.global.carting.id;
        this.item=this.global.carting.product;
        this.global.price=this.global.carting.product;
        console.log(this.item[0].stock_status);
      //  this.productdetail=this.data;
       console.log(this.item);
    //  console.log(this.data.subcategory);
      
    
       console.log(this.global.storecartdetails.count);
              
       this.global.adding();
  
  
  this.loaded="1";
       }, error => {
       
       })
  }


  gotowish(){
   
    this.router.navigateByUrl('/mmwishlist');
} 
gotocart(){
 
    this.router.navigateByUrl('/mmnewcarts');
}

async address(){

  for(let i=0; i<this.item.length; i++){
    console.log(this.item[i].stock_status);

  if(this.item[i].stock_status=='outofstock'){

    const toast = await this.toastController.create({
      message: 'Remove The Unavailable Products.',
      duration: 2000,
      position: 'middle',
      color:'primary'
    });
    toast.present();
    return;
 
  }

}

// if(this.global.useraddress){
//   this.router.navigateByUrl('/addaddress');
// }else if(this.global.state=='1'){
//   this.router.navigateByUrl('/address');
// }else{
//   // tslint:disable-next-line: no-unused-expression
//   this.global.flack='1';
//   this.router.navigateByUrl('/login');

// }


if(this.global.state=='1'){
  this.router.navigateByUrl('/address');
}else{
  // tslint:disable-next-line: no-unused-expression
  this.global.flack='1';
  this.router.navigateByUrl('/login');

}
}

}
