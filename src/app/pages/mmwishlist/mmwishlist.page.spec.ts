import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmwishlistPage } from './mmwishlist.page';

describe('MmwishlistPage', () => {
  let component: MmwishlistPage;
  let fixture: ComponentFixture<MmwishlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmwishlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmwishlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
