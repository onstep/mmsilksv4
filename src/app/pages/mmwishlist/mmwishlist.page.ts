import { Component, OnInit,ViewChild } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { FivFeature } from '@fivethree/core';

@Component({
  selector: 'app-mmwishlist',
  templateUrl: './mmwishlist.page.html',
  styleUrls: ['./mmwishlist.page.scss'],
})
export class MmwishlistPage implements OnInit {
wishid:any;
  productdetail: any;
  data: any;
  loaded:any=0;
  wishlistdata:any;
  lottieConfig;
  private anim: any;
  @ViewChild('feature') feature: FivFeature;

  constructor(private activeRoute: ActivatedRoute,public global: GlobalService,private router: Router) { }

  ngOnInit() {
    this.lottieConfig = {
      path: 'assets/emptys.json',
     renderer: 'canvas',
      autoplay: true,
      loop: false
  };
    this.global.storage.get('wishlist').then((val) => {
      console.log(val);
      this.wishlistdata=val;
      console.log(this.wishlistdata);

    //alert(val.length);
  var total:number=0;
  var totalmrp:number=0;
  var totalcount:number=0;
  
 
         this.initial();
  
     // alert("price"+this.storecartdetails.price);
      //alert("price"+this.storecartdetails.count);
      //alert("price"+this.storecartdetails.totalcount);
    });
  }
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

 //   let idarray=[{id:"5182"},{id:"5205"}]
    let idarray=this.wishlistdata;
    let postData =  { id:idarray }
    console.log('postData');
    console.log(postData);

    this.global.http.post(this.global.url+"wishlist.php", postData,{observe: 'response'})      
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.global.wishing = data.body;
      //  this.productdetail=this.data;
       console.log(this.global.wishing.product);
    //  console.log(this.data.subcategory);
      
      

              
  
  
  this.loaded="1";
       }, error => {
       
       })
  }
  gotowish(){
   
    this.router.navigateByUrl('/mmwishlist');
} 
gotocart(){
 
    this.router.navigateByUrl('/mmnewcarts');
}
}
