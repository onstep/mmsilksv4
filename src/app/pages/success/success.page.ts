import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { async } from 'q';

@Component({
  selector: 'app-success',
  templateUrl: './success.page.html',
  styleUrls: ['./success.page.scss'],
})
export class SuccessPage implements OnInit {
orderid:any;
lottieConfig;
private anim: any;
succ;
status;
  constructor(private activeRoute: ActivatedRoute,public loadingController: LoadingController) { }

  ngOnInit() {
    this.orderid = this.activeRoute.snapshot.paramMap.get('orderid');
    this.lottieConfig = {
      path: 'assets/tickwhite.json',
     renderer: 'canvas',
      autoplay: true,
      loop: false
  };



  }
 
// OnDestroy(){
//   clearInterval(this.succ);

//   }
  
//   handleAnimation(anim: any) {
//     this.anim = anim;
//     this.status=1;
//     this.succ= setInterval( ()=>{

//       if(this.status==1){
// this.stop();
// this.status=0;
//       }else{
//         this.play();
//         this.status=1;

//       }

//       console.log('123');
//   },3500);

//   }

//   stop() {
//     this.anim.stop();
//     this.status=0;

//   }

//   play() {
//     this.anim.play();
//     this.status=0;

//   }

}
