import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FivFeature } from '@fivethree/core';
import { ModalController,NavController,Platform } from '@ionic/angular';
// import { HTTP } from '@ionic-native/http/ngx';
// import { MmcategoryPage} from '../../pages/mmcategory/mmcategory';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-mmhome',
  templateUrl: './mmhome.page.html',
  styleUrls: ['./mmhome.page.scss'],
})
export class MmhomePage implements OnInit {
  category:any;
  data:any;
  catg:any;
  products:any;
  banner:any;
  featured:any;
  offer:any;
  loaded:any='0';
  titleMode = 'hide';
  position = 'center';
  totalproducts:any;
  cate:any;
  subcat:any;
  // Mmcategory=MmcategoryPage;

  

  @ViewChild('feature') feature: FivFeature;
  constructor(private navCtrl: NavController,public global: GlobalService,private router: Router) { }

  ngOnInit() {
    this.initial();
        
    console.log(this.global.customerid);

    console.log(this.global.useraddress);
    console.log(this.global.storecartdetails.count);

  }
  initial()
  {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { userid: '1'}

    this.global.http.post(this.global.url+"index.php", postData,{observe: 'response'})      
 
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data.body;
       this.products=this.data.categories;
      //  this.totalproducts=this.data.subcat;
      //  console.log(this.totalproducts);

       this.banner=this.data.banner;
       this.featured=this.data.featuredproduct;
        this.offer=this.data.offer;
       console.log(this.featured);
      
       console.log(this.global.storecartdetails.count);
       console.log("cate");

    //    for (let i=0; i<this.totalproducts.length; i++){
       
    //     console.log("cate");
    //     if(this.totalproducts[i].parent=='0'){
    //  this.cate=this.totalproducts[i];
    //  console.log(this.cate);
    //     }

    //     else{
    //      this.subcat=this.totalproducts[i];
    //      console.log(this.subcat);
     
    //     }

    //   }      
    //   console.log(this.cate);
  
  
  this.loaded="1"
       }, error => {
       
       })
  }

 
  gotowish(){
   
    this.router.navigateByUrl('/mmwishlist');
} 
gotocart(){
 
    this.router.navigateByUrl('/mmnewcarts');
}
gosearch(){
 
  this.router.navigateByUrl('/mmsearch');
}

}
