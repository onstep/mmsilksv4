import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '@components/components.module';

import { IonicModule } from '@ionic/angular';

import { MmhomePage } from './mmhome.page';
import { FivFeatureDiscoveryModule } from '@fivethree/core';
import { FivIconModule } from '@fivethree/core';
import { FivAppBarModule } from '@fivethree/core';

const routes: Routes = [
  {
    path: '',
    component: MmhomePage
  }
];

@NgModule({
  imports: [
    
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes),
    FivFeatureDiscoveryModule,
    FivIconModule,
    FivAppBarModule,
  ],
  declarations: [MmhomePage]
})
export class MmhomePageModule {}
