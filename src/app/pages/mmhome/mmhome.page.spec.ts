import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmhomePage } from './mmhome.page';

describe('MmhomePage', () => {
  let component: MmhomePage;
  let fixture: ComponentFixture<MmhomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmhomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmhomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
