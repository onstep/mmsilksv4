import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MmpreorderPage } from './mmpreorder.page';

const routes: Routes = [
  {
    path: '',
    component: MmpreorderPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MmpreorderPage]
})
export class MmpreorderPageModule {}
