import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmpreorderPage } from './mmpreorder.page';

describe('MmpreorderPage', () => {
  let component: MmpreorderPage;
  let fixture: ComponentFixture<MmpreorderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmpreorderPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmpreorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
