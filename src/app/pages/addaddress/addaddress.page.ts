import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addaddress',
  templateUrl: './addaddress.page.html',
  styleUrls: ['./addaddress.page.scss'],
})
export class AddaddressPage implements OnInit {

  constructor(private router: Router,public global: GlobalService) { }

  ngOnInit() {
  }
  checkout(){
    this.router.navigateByUrl('/checkout');

  }
}
