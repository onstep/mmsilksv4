import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddresspickerPage } from './addresspicker.page';

const routes: Routes = [
  {
    path: '',
    component: AddresspickerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddresspickerPage],
 // entryComponents  : [AddresspickerPage],

})
export class AddresspickerPageModule {}
