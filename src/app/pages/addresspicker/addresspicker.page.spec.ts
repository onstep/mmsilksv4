import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddresspickerPage } from './addresspicker.page';

describe('AddresspickerPage', () => {
  let component: AddresspickerPage;
  let fixture: ComponentFixture<AddresspickerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddresspickerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddresspickerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
