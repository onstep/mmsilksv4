/// <reference types="@types/googlemaps" />
import { Component, OnInit } from '@angular/core';
import { NgZone } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ModalController,Platform,NavParams } from '@ionic/angular';
import { GlobalService } from '../../services/global.service'
@Component({
  selector: 'app-addresspicker',
  templateUrl: './addresspicker.page.html',
  styleUrls: ['./addresspicker.page.scss'],
})
export class AddresspickerPage implements OnInit {
  autocompleteItems;
  autocomplete;

  latitude: number = 0;
  longitude: number = 0;
  geo: any
  type: any
  city: any
ready:any=0;
  service = new google.maps.places.AutocompleteService();
  constructor (public global:GlobalService,public navParams: NavParams,public viewCtrl: ModalController,private storage: Storage,private zone: NgZone) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
    console.log(this.service);
  }
  ngOnInit(){
    this.type=this.navParams.get('type');
    this.ready=1;
  }
  dismiss() {
   this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {
   // this.viewCtrl.dismiss(item);
    this.geo = item;
    this.geoCode(this.geo);//convert Address to lat and long
  }

  updateSearch() {

    if (this.autocomplete.query == '') {
     this.autocompleteItems = [];
     return;
    }

    let me = this;
    this.service.getPlacePredictions({
    input: this.autocomplete.query,
    componentRestrictions: {
      country: 'IN'
    }
   }, (predictions, status) => {
     me.autocompleteItems = [];

   me.zone.run(() => {
     if (predictions != null) {
        predictions.forEach((prediction) => {
          me.autocompleteItems.push(prediction.description);
        });
       }
     });
   });
  }

  //convert Address string to lat and long
  geoCode(address:any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      console.log(results);
    this.latitude = results[0].geometry.location.lat();
    this.longitude = results[0].geometry.location.lng();
    this.city = results[0].address_components[1].long_name;

    for (var i=0; i<results[0].address_components.length; i++) {
      for (var b=0;b<results[0].address_components[i].types.length;b++) {

          if (results[0].address_components[i].types[b] == "administrative_area_level_2") {
           // alert(results[0].address_components[i].long_name);
            var mycity =results[0].address_components[i].long_name;
          }
          if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
            //alert(results[0].address_components[i].long_name);
            var mystate =results[0].address_components[i].long_name;
          }
      }
    }

   // alert("lat: " + this.latitude + ", long: " + this.longitude);
    var data={lat:this.latitude,lng:this.longitude,address:address,type:this.type,city: mycity,state: mystate};
   // alert(this.type);
    if(this.type==='from'){
 this.global.from=data;
//alert();
    }
    if(this.type==='to'){
 this.global.to=data;
// alert();

    }
 //   this.storage.set('from',data);
 console.log(this.global);
    this.viewCtrl.dismiss(data);
/*
    // Or to get a key/value pair
    this.storage.get('age').then((val) => {
      console.log('Your age is', val);
    });
    */
  });
 }
}