import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
// import { CallNumber } from '@ionic-native/call-number/ngx';

import { MmaddressPage } from './mmaddress.page';
const routes: Routes = [
  {
    path: '',
    component: MmaddressPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // CallNumber,
    RouterModule.forChild(routes)
  ],
  declarations: [MmaddressPage]
})
export class MmaddressPageModule {}
