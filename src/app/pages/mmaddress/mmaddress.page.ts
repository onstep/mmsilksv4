import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

// import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-mmaddress',
  templateUrl: './mmaddress.page.html',
  styleUrls: ['./mmaddress.page.scss'],
})
export class MmaddressPage implements OnInit {

  constructor(public toastController: ToastController,public alertController: AlertController,public global: GlobalService, private router: Router,private activeRoute: ActivatedRoute,private storage: Storage) { }

  ngOnInit() {
    console.log( this.global.customerrole);
    console.log( this.global.useraddress);
  }
  async  logout(){
  

     
      const alert = await this.alertController.create({
        header: 'Are You Sure ?',
        // message: 'Message <strong>text</strong>!!!',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Logout',
            handler: () => {
              var userid="nouserid";
              this.storage.set('userid', userid);
              this.storage.remove('userid');
              this.storage.remove('role');
              this.storage.remove('address');

              console.log( this.global.customerid);
              this.global.customerid=null;        
              this.global.customerrole=null;        
              this.global.useraddress=null;        
              console.log( this.global.customerrole);
              console.log( this.global.useraddress);

              this.storage.get('userid').then(async (val) => {

                this.global.state='0';
                console.log( this.global.customerid);
        
                console.log( this.global.customerrole);
  

                this.router.navigateByUrl('/myhome/tabs/tab1');

                const toast = await this.toastController.create({
                  message: 'Logged Out Sucessfully',
                  duration: 3000,
                  color:'myred',
                  position: 'bottom',
                });
                toast.present();

                console.log(val);
                 });
            }
          }
        ]
      });
  
      await alert.present();
  

   



   
}
// call(){
//   this.callNumber.callNumber("9578188598", true)
// .then(res => console.log('Launched dialer!', res))
// .catch(err => console.log('Error launching dialer', err));
// }
login(){
  this.router.navigateByUrl('/login');
  this.global.state='1';
}

}
