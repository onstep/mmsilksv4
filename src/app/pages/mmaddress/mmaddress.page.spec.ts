import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmaddressPage } from './mmaddress.page';

describe('MmaddressPage', () => {
  let component: MmaddressPage;
  let fixture: ComponentFixture<MmaddressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmaddressPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmaddressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
