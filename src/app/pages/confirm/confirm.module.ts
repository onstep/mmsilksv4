import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FivCenterModule } from './../../../../projects/core/src/lib/center/center.module';
import { FivRippleModule } from './../../../../projects/core/src/lib/ripple/ripple.module';
import { FivExpandableModule } from '@fivethree/core';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from '@components/components.module';
import { NgxThanosModule } from '@wellwind/ngx-thanos';
import { ParticleEffectButtonModule } from "angular-particle-effect-button";

import { ConfirmPage } from './confirm.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FivExpandableModule,
    FivRippleModule,
    FivCenterModule,
    ComponentsModule,
    NgxThanosModule,
    ParticleEffectButtonModule,
        RouterModule.forChild(routes)
  ],
  declarations: [ConfirmPage]
})
export class ConfirmPageModule {}
