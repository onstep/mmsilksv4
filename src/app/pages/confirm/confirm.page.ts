import { AlertController } from '@ionic/angular';
import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { FivDialog } from '@fivethree/core';
import { ModalController,NavController,Platform } from '@ionic/angular';
import { GlobalService } from '../../services/global.service';
import { Storage } from '@ionic/storage';
import { ThanosSnapService } from '@wellwind/ngx-thanos';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.page.html',
  styleUrls: ['./confirm.page.scss'],
})
export class ConfirmPage implements OnInit {
  snap = false;
  canRewind = false;
  @ViewChild('dialog') dialog: FivDialog;
  backdrop = true;
  pull = true;
  verticalAlign = 'bottom';
  horizontalAlign = 'left';
  shape = 'card';
  duration = 0;
  inDuration = '220';
  outDuration = '180';
  estimated=0;
sharing:any=false;
km;
formdata:any={};
loaded=0;
count=0;
price=0;
hidden=false;
shareloading=false;
coupondata={};
couponapplied=false;
buttondis=false;
firstprice;
firstsharingprice;
onload:any=0;

  constructor(private thanosSnapService: ThanosSnapService,private storage: Storage,public alertController: AlertController,private router: NavController, public global: GlobalService) { }
promospin:any=0;
  ngOnInit() {
     console.log(this.global);
    this.storage.get('sharing').then((val) => {
      console.log('sharing');
      console.log(val);
    if(val){
    //  this.sharing=val;
    }
   
      this.storage.get('km').then((km) => {
       
      if(km){
        this.km=km;
      }
      
      this.storage.get('things').then((things) => {
       
        if(things){
          this.global.items=things;
          let count=0;
          //console.log(count);

          for(let y=0;y<this.global.items.length;y++){
            console.log(this.global.items[y].count);
            count= count+this.global.items[y].count;
            console.log('count');
            console.log(count);
          }
          this.count=count;
        }
        this.getestimate();
        });
      });
    });
  
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Are you sure to book!',
      message: '* Warning content one.<br>* Warning content one <br>* Warning content one ',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'dark',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Book Now',
          cssClass: 'dark',
          handler: () => {
            this.booknow();
           // this.router.navigateForward('/success');
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  async enableshaing() {
    console.log('yy');
    console.log(this.sharing);

    if(this.sharing==false){
// this.sharing=false;
this.hidden=true;
this.formdata.sharing=this.sharing;

var timer34 = setTimeout(() => {
  // alert();
 this.price=this.firstprice;
  this.hidden=false;

  // console.log(this.price);
  
 }, 2500);
// this.estimate2();
    }else{

    
    const alert = await this.alertController.create({
      header: 'Are you sure to enable!',
      message: '* Warning content one.<br>* Warning content one <br>* Warning content one ',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'dark',
          handler: (blah) => {
            this.sharing=false;
            console.log(this.sharing);

            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirm',
          cssClass: 'dark',
          handler: () => {
            this.hidden=true;

            var timer34 = setTimeout(() => {
              // alert();
              this.price=this.firstsharingprice;
              this.hidden=false;
              this.formdata.sharing=this.sharing;


              // console.log(this.price);
              
             }, 2500);
            // this.hidden=true;
            // this.sharing=true;
          //  this.router.navigateForward('/success');
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
  }
//   async enableshaing() {
//     console.log('yy');
//     console.log(this.sharing);

//     if(this.sharing==false){
// // this.sharing=false;
// this.estimate2();
//     }else{

    
//     const alert = await this.alertController.create({
//       header: 'Are you sure to enable!',
//       message: '* Warning content one.<br>* Warning content one <br>* Warning content one ',
//       buttons: [
//         {
//           text: 'Cancel',
//           role: 'cancel',
//           cssClass: 'dark',
//           handler: (blah) => {
//             // this.sharing=false;
//             console.log(this.sharing);

//             console.log('Confirm Cancel: blah');
//           }
//         }, {
//           text: 'Confirm',
//           cssClass: 'dark',
//           handler: () => {
//             this.estimate2();
//             // this.hidden=true;
//             // this.sharing=true;
//           //  this.router.navigateForward('/success');
//             console.log('Confirm Okay');
//           }
//         }
//       ]
//     });

//     await alert.present();
//   }
//   }

  getestimate(){
    this.hidden=true;
    this.buttondis=true;
    this.promospin=true;

    // alert(this.sharing);
     this.storage.set('sharing',this.sharing);
 this.formdata.userid="1";
 this.formdata.distance=this.km.value;
 this.formdata.fromaddress=this.global.from;
 this.formdata.toaddress=this.global.to;
 this.formdata.sharing=this.sharing;
 this.formdata.things=this.global.items;
 this.formdata.form=this.global.mappageform;
 if(this.formdata.sharing==false){
  // alert();
  this.formdata.sharing=0;
 }
 if(this.formdata.sharing==true){
  this.formdata.sharing=1;
 }
 console.log(this.formdata);
//  this.navCtrl.navigateForward('/confirm')
 
     // {userid:1,distance:500,city:a,
     //   fromaddress:{lat:19.34,lng:23.99,address:"Kothaval chanvaid street",lift:true,floor:2},
     //   toaddress:{lat:19.34,lng:23.99,address:"ramnad",lift:false,floor:3},
     //   things: [{id:1,name:"Kitchen table",count:5,image:"assets/washing.svg",categoryid:"2"},
     //           {id:1,name:"Refrigerator",count:0,image:"assets/washing.svg",categoryid:"2"},
     //           {id:1,name:"Toaster",count:4,image:"assets/washing.svg",categoryid:"2"},
     //           {id:1,name:"Microwave",count:9,image:"assets/washing.svg",categoryid:"2"}],
     //  sharing:true}
  // this.navCtrl.navigateForward('/confirm')
 
  var headers = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json' );
 
 
  let postData =  this.formdata;
 
  this.global.http.post(this.global.url+"estimation/new.php", postData,{observe: 'response'})
    .subscribe(data => {
      console.log(data.body);
      // tslint:disable-next-line: triple-equals

      if(data.body['status']=='success'){
 this.estimated=1;
    this.shareloading=false;

 this.formdata.orderid=data.body['orderid'];
 this.promospin=false;
 this.couponapplied=false;


 if(this.onload==0){
  this.firstprice=data.body['price'];
  this.firstsharingprice=data.body['sharing'];
  this.price=data.body['price'];
  this.hidden=false;
  this.buttondis=false;
  this.onload=1;

 }else{
  var timer34 = setTimeout(() => {
    // alert();
    this.firstprice=data.body['price'];
    this.firstsharingprice=data.body['sharing'];
    this.price=data.body['price'];
    this.hidden=false;
    this.buttondis=false;
  
    // console.log(this.price);
    
   }, 2500);
 }
 
 this.loaded=1;

//this.animateValue( 0, data.body['price'], 200);
 //alert(this.price);
      }
     }, error => {
      console.log(error);
    });
    
 
   }
  estimate2(){
    // alert(this.sharing);
    this.shareloading=true;
    this.hidden=true;
     this.storage.set('sharing',this.sharing);
 this.formdata.userid="1";
 this.formdata.distance=this.km.value;
 this.formdata.fromaddress=this.global.from;
 this.formdata.toaddress=this.global.to;
 this.formdata.sharing=this.sharing;
 this.formdata.things=this.global.items;
 this.formdata.form=this.global.mappageform;
 if(this.formdata.sharing==false){
  // alert();
  this.formdata.sharing=0;
 }
 if(this.formdata.sharing==true){
  this.formdata.sharing=1;
 }
 console.log(this.formdata);
//  this.navCtrl.navigateForward('/confirm')
 
     // {userid:1,distance:500,city:a,
     //   fromaddress:{lat:19.34,lng:23.99,address:"Kothaval chanvaid street",lift:true,floor:2},
     //   toaddress:{lat:19.34,lng:23.99,address:"ramnad",lift:false,floor:3},
     //   things: [{id:1,name:"Kitchen table",count:5,image:"assets/washing.svg",categoryid:"2"},
     //           {id:1,name:"Refrigerator",count:0,image:"assets/washing.svg",categoryid:"2"},
     //           {id:1,name:"Toaster",count:4,image:"assets/washing.svg",categoryid:"2"},
     //           {id:1,name:"Microwave",count:9,image:"assets/washing.svg",categoryid:"2"}],
     //  sharing:true}
  // this.navCtrl.navigateForward('/confirm')
 
  var headers = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json' );
 
 
  let postData =  this.formdata;
 
  this.global.http.post(this.global.url+"estimation/new.php", postData,{observe: 'response'})
    .subscribe(data => {
      console.log(data.body);
      // tslint:disable-next-line: triple-equals
      if(data.body['status']=='success'){
 this.estimated=1;
 //this.hidden=true;
 var timer = setTimeout(() => {
  this.price=data.body['price'];
 // alert();
 this.hidden=false;
 this.shareloading=false;
 console.log('jjj');
 console.log(this.sharing);
 // console.log(this.price);
 
}, 2500);
 this.loaded=1;

//this.animateValue( 0, data.body['price'], 200);
 //alert(this.price);
      }
     }, error => {
      console.log(error);
    });
    
 
   }


   applyoffer(){
    this.hidden=true;
    this.buttondis=true;
    // alert(this.sharing);
    if(!this.formdata.coupon){
alert("Please enter the coupon");
this.hidden=false;
this.buttondis=false;

return;
    }
    this.promospin=true;
     this.storage.set('sharing',this.sharing);
 this.formdata.userid="1";
 this.formdata.price=this.price;
 if(this.formdata.sharing==false){
  // alert();
  this.formdata.sharing=0;
 }
 if(this.formdata.sharing==true){
  this.formdata.sharing=1;
 }
//  this.formdata.distance=this.km.value;
//  this.formdata.fromaddress=this.global.from;
//  this.formdata.toaddress=this.global.to;
//  this.formdata.sharing=this.sharing;
//  this.formdata.things=this.global.items;
//  this.formdata.form=this.global.mappageform;
 
 console.log(this.formdata);

 
  var headers = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json' );
 
 
  let postData =  {formdata:this.formdata};
 
  this.global.http.post(this.global.url+"estimation/promo.php", postData,{observe: 'response'})
    .subscribe(data => {
      console.log(data.body);
      // tslint:disable-next-line: triple-equals
      if(data.body['status']=='success'){
        if(data.body['applied']=='yes'){
                  this.coupondata=data.body;
                  this.couponapplied=true;
                  this.promospin=false;
                  var timer = setTimeout(() => {
                    this.price=data.body['value'];
                   // alert();
                   this.hidden=false;
                   this.buttondis=false;

                   // console.log(this.price);
                   
                  }, 2500);

        }else{
          alert(data.body['message']);
          this.promospin=false;
          var timer3 = setTimeout(() => {
           // alert();
           this.hidden=false;
           this.buttondis=false;

           // console.log(this.price);
           
          }, 2500);
        }

      }
     }, error => {
      console.log(error);
    });
    
 
   }


   booknow(){
    this.buttondis=true;
  
     this.storage.set('sharing',this.sharing);
 this.formdata.userid="1";
 this.formdata.id=this.formdata.orderid;
 this.formdata.price=this.price;
 if(this.formdata.sharing==false){
  // alert();
  this.formdata.sharing=0;
 }
 if(this.formdata.sharing==true){
  this.formdata.sharing=1;
 }
//  this.formdata.distance=this.km.value;
//  this.formdata.fromaddress=this.global.from;
//  this.formdata.toaddress=this.global.to;
//  this.formdata.sharing=this.sharing;
//  this.formdata.things=this.global.items;
//  this.formdata.form=this.global.mappageform;
 
 console.log(this.formdata);

 
  var headers = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json' );
 
 
  let postData =  this.formdata;
 
  this.global.http.post(this.global.url+"estimation/confirmbooking.php", postData,{observe: 'response'})
    .subscribe(data => {
      console.log(data.body);
      // tslint:disable-next-line: triple-equals
      if(data.body['status']=='success'){
   this.router.navigateRoot('/success/'+data.body['orderid']);


      }
     }, error => {
      console.log(error);
    });
    
 
   }

   ngAfterViewInit() {

   }

  animateValue( start, end, duration) {
   // console.log(start);
   // console.log(end);
    var range = end - start;
    var current = start;
    var increment = end > start? 1 : -1;
    var stepTime = Math.abs(Math.floor(duration / range));
   console.log('stepTime');
   console.log(stepTime);
    var timer = setInterval(() => {
        current += increment;
       // console.log(current);
        this.price=current;
       // console.log(this.price);
        if (current == end) {
            clearInterval(timer);
        }
    }, 3);
  }


}
