import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmsucessPage } from './mmsucess.page';

describe('MmsucessPage', () => {
  let component: MmsucessPage;
  let fixture: ComponentFixture<MmsucessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmsucessPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmsucessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
