import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import {  Platform, NavController } from '@ionic/angular';
@Component({
  selector: 'app-mmsucess',
  templateUrl: './mmsucess.page.html',
  styleUrls: ['./mmsucess.page.scss'],
})
export class MmsucessPage implements OnInit {
  orderid:any;
  lottieConfig;
  private anim: any;
  succ;
  status;
  item:any;
  id: any;
  constructor(private platform: Platform,private navctrl: NavController,private activeRoute: ActivatedRoute,private router: Router,private storage: Storage,public global: GlobalService) { }

  ngOnInit() {
    this.global.removestorecart();

    this.orderid = this.activeRoute.snapshot.paramMap.get('orderid');
    this.lottieConfig = {
      path: 'assets/tickblue.json',
     renderer: 'canvas',
      autoplay: true,
      loop: false
  };
  // this.platform.ready().then(()=>{
  //   this.backbuttonevent();
  //     }
  //     );
  
 
  }
  
    remove(){
     
      const storecartdata=[];
      this.storage.set('storecart', storecartdata);

   }
   backbuttonevent(){
    this.router.navigateByUrl('/');
  } 
  }

  





// OnDestroy(){
//   clearInterval(this.succ);
//   }
//   handleAnimation(anim: any) {
//     this.anim = anim;
//     this.status=1;
//     this.succ= setInterval( ()=>{
//       if(this.status==1){
// this.stop();
// this.status=0;
//       }else{
//         this.play();
//         this.status=1;
//       }
//       console.log('123');
//   },3500);
//   }
//   stop() {
//     this.anim.stop();
//     this.status=0;
//   }
//   play() {
//     this.anim.play();
//     this.status=0;
//   }
