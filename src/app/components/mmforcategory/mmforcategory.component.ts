import { Component, OnInit,Input,ViewChild, } from '@angular/core';
import { Router } from '@angular/router';
import {GlobalService } from '../../services/global.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-mmforcategory',
  templateUrl: './mmforcategory.component.html',
  styleUrls: ['./mmforcategory.component.scss'],
})
export class MmforcategoryComponent implements OnInit {
  category: any;
  loaded: number;
  @Input('data') data: any;


  constructor(private router: Router,public global: GlobalService,private activeRoute: ActivatedRoute) { }

  ngOnInit() {
console.log(this.data);
  }
  subcategories(id){
    this.activeRoute.snapshot.paramMap.get('id');
    this.router.navigateByUrl('/mmsubategory');
  }
  
}

