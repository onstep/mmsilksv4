import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { Storage } from '@ionic/storage';
import { empty } from 'rxjs';

@Component({
  selector: 'app-forproductlist',
  templateUrl: './forproductlist.component.html',
  styleUrls: ['./forproductlist.component.scss'],
})
export class ForproductlistComponent implements OnInit {
  @Input('id') id: any;
  @Input('img') img: any;
  @Input('name') name: any;
  @Input('price') price: any;
  @Input('delprice') delprice: any;
  @Input('stock') stock: any;
  className: string;
  count: any;
  categoryid: any;
  wishlisted:any=0;
  constructor(private router: Router,public global: GlobalService,private storage: Storage) { }
idarray:any;
data:any;
loaded:any=[];
productdetails:any;
lottieConfig;
private anim: any;

  ngOnInit() {
    this.lottieConfig = {
      path: 'assets/heart.json',
     renderer: 'svg',
      autoplay: true,
      loop: false
  };

    this.iswishlist();
   }
   iswishlist(){
      ///retrive the wishlist data from localstorage
    this.storage.get('wishlist').then((val) => {
  //console.log(val);
  var  index = val.findIndex((obj => obj.productid == this.id));
  //if already in list 
  if(index == -1){
    this.wishlisted=0;

  }
  else{
    this.wishlisted=1;
 
  
  }
  });
   }


  productdetail(id){
    this.router.navigateByUrl('/mmproductdetails/'+id)
  }


  productcount(){

    this.storage.get('storecart').then((val) => {
  //console.log(val);
  var  index = val.findIndex((obj => obj.productid == this.productdetails.id));
  
   if(index == -1){
    this.count=0;
  
  }
  else{
  this.count=val[index].count;
  
  }
  });
   }
  wishlist(event){
    event.stopPropagation();

  let productid=this.id;
   //retrive the wishlist data from localstorage 
    this.storage.get('wishlist').then((val) => {

      console.log(val);
   //it will return -1 if product not addeded in wishlist else return the index of the product

    var  index = val.findIndex((obj => obj.productid == productid));

    if(index== -1){
      //declare the array with product id
   let item ={productid: productid};
  
//push the declared data to the array from localstorage
  val.push(item);
  ///update the new array to localstorage
this.storage.set('wishlist',val).then((val) => {
  this.iswishlist();

  ///set the value in localstorage
  this.storage.get('wishlist').then((val) => {
    console.log(val);

  });
});
 
    }
    else{
     // delete val[index];
      console.log(val);
      //if already added remove  element the array
      val.splice(index, 1);

      ///update the new array to localstorage
    this.storage.set('wishlist',val).then((val) => {
      this.iswishlist();

      ///set the value in localstorage
      this.storage.get('wishlist').then((val) => {
        console.log(val);
    
      });
    });
    }
  });
 
   
}
}


// this.storage.set('name','ravisankar' );
// this.storage.set('number','99' );
// this.storage.get('name' ).then((mydata)=>{
//   var name=mydata;

// // console.log('your name is',name);
// this.storage.get('number' ).then((mydata)=>{
//   var number=mydata;
//   // console.log('your number is',number);
//   console.log('your name is',name , 'your number is', number);

//   });

// });

