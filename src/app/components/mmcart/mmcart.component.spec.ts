import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmcartComponent } from './mmcart.component';

describe('MmcartComponent', () => {
  let component: MmcartComponent;
  let fixture: ComponentFixture<MmcartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmcartComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmcartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
