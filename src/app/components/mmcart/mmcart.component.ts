import { Component, OnInit,Input,ViewChild } from '@angular/core';
import {GlobalService } from '../../services/global.service';
import { Storage } from '@ionic/storage';
import { Platform, Events, IonSelect } from '@ionic/angular';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-mmcart',
  templateUrl: './mmcart.component.html',
  styleUrls: ['./mmcart.component.scss'],
})
export class MmcartComponent implements OnInit {
  @ViewChild('count') countselect: IonSelect;
  showcountselect = true;

  @Input('id') id: any;
  @Input('img') img: any;
  @Input('name') name: any;
  @Input('price') price: any;
  @Input('delprice') delprice: any;
  @Input('stock') stock: any;
  @Input('stockquantity') stockquantity: any;
  @Input('description') description: any;
  itemcount:number;
  loaded:any=0;
  
  constructor(public toastController: ToastController,public global: GlobalService,private storage: Storage,private router: Router) { }

  ngOnInit() {
  

 this.counting();
 this.updatecount();
console.log(this.stock);
  }
  // async quantity() {
  //   const toast = await this.toastController.create({
  //     message: 'Your settings have been saved.',
  //     duration: 2000
  //   });
  //   toast.present();
  // }

  updatecount(){
    this.storage.get('storecart').then((val) => {
      //find the index/position of product id
         var  index = val.findIndex((obj => obj.productid == this.id));
         //alert(index);
       
        //  val[index].count=this.itemcount;
       //update the array with itemcount
        val[index].count=this.itemcount;
       //update the storecart(localstorage) with updated array
        this.storage.set('storecart',val);
      
     });    
  }
  counting(){
    this.storage.get('storecart').then((val) => {
   //find the index/position of product id
      var  index = val.findIndex((obj => obj.productid == this.id));
      //alert(index);
    
       this.itemcount=val[index].count;
       this.loaded=1;
     //  alert(this.count);
    
   
  });    
   }
  close($event) {
    $event.stopPropagation();

    this.countselect.open()
  
    };
    saving($event){
      $event.stopPropagation();
      this.save();

    }
    rm($event){
      $event.stopPropagation();
      this.remove();
      this.global.remove="1";
      console.log(this.global.remove);
     
    }
    remove(){
     
      this.global.storage.get('storecart').then((val)=> {
      console.log(val);
      var  index = val.findIndex((obj => obj.productid == this.id));
 
if(index!=-1){

  val.splice(index, 1);
  this.global.carting.product.splice(index, 1);


  console.log(val);
  this.storage.set('storecart',val).then((val) => {
  console.log(val);
  this.global.storecartdetails.count=  this.global.storecartdetails.count-1;
  this.global.adding();
  

  });

}

});

 }

 save(){

let productid=this.id;
 //retrive the wishlist data from localstorage 
  this.storage.get('wishlist').then((val) => {

    console.log(val);
 //it will return -1 if product not addeded in wishlist else return the index of the product

  var  index = val.findIndex((obj => obj.productid == productid));

  if(index== -1){
    //declare the array with product id
 let item ={productid: productid};

//push the declared data to the array from localstorage
val.push(item);
///update the new array to localstorage
this.storage.set('wishlist',val).then((val) => {
// this.iswishlist();

///set the value in localstorage
this.storage.get('wishlist').then((val) => {
  console.log(val);

});
});

  }
  else{
   // delete val[index];
    console.log(val);
    //if already added remove  element the array
    val.splice(index, 1);

    ///update the new array to localstorage
  this.storage.set('wishlist',val).then((val) => {
    // this.iswishlist();

    ///set the value in localstorage
    this.storage.get('wishlist').then((val) => {
      console.log(val);
  
    });
  });
  }
});
this.remove();
 
}
detail(id){
  this.router.navigateByUrl('/mmproductdetails/'+id);

}
}
