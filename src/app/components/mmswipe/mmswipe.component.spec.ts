import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmswipeComponent } from './mmswipe.component';

describe('MmswipeComponent', () => {
  let component: MmswipeComponent;
  let fixture: ComponentFixture<MmswipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmswipeComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmswipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
