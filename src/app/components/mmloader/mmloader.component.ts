import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mmloader',
  templateUrl: './mmloader.component.html',
  styleUrls: ['./mmloader.component.scss'],
})
export class MmloaderComponent implements OnInit {
  lottieConfig;
  private anim: any;
  constructor() { }

  ngOnInit() {
    this.lottieConfig = {
      path: 'assets/fronting.json',
     renderer: 'svg',
      autoplay: true,
      loop: true
  };
  }

}
