import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mmproductlist',
  templateUrl: './mmproductlist.component.html',
  styleUrls: ['./mmproductlist.component.scss'],
})
export class MmproductlistComponent implements OnInit {
  category: any;
  @Input('data') data: any;

  constructor(private router: Router) { }

  ngOnInit() {

  }
  productdetails(){
    this.router.navigateByUrl('/mmproductdetails');

  }

}
