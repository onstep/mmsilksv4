import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mmforsubcategory',
  templateUrl: './mmforsubcategory.component.html',
  styleUrls: ['./mmforsubcategory.component.scss'],
})
export class MmforsubcategoryComponent implements OnInit {
  @Input('data') data: any;

  constructor(private router: Router) { }

  ngOnInit() {}
  productlist(){
    this.router.navigateByUrl('/mmproductlisting');
    

  }
}
