import { Component, OnInit,Input,ViewChild, } from '@angular/core';
import { Router } from '@angular/router';
import {GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-mmsubcategory',
  templateUrl: './mmsubcategory.component.html',
  styleUrls: ['./mmsubcategory.component.scss'],
})
export class MmsubcategoryComponent implements OnInit {
category:any;
@Input('id') id: any;
@Input('name') name: any;
@Input('img') img: any;
@Input('slot') slot: any;
@Input('description') description: any;

  constructor(private router: Router,public global: GlobalService ) {
   }

  ngOnInit() {}
  
 
  loaddata(id){

     
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { userid: 'id'}

    this.global.http.post(this.global.url+"index.php", postData,{observe: 'response'})
      .subscribe(data => {
        // console.log(data.body);
        var ddata=data.body;
        // tslint:disable-next-line: triple-equals
        if(ddata['status']=='success'){
//alert('insert otp ');
// this.things=ddata['items'];
// console.log(this.things);
this.category=ddata['subcategory'];
console.log(this.category[0].image.src);

// this.loaded=1;  
setTimeout(() => {
  //this.addfiv.show();
}, 2000);
//this.things=this.global.items;
        }
       }, error => {
        console.log(error);
      });
  }
  productlist(id,name){
    this.router.navigateByUrl('/mmproductlisting/'+id+'/'+name);
    console.log(id);
  }
}
