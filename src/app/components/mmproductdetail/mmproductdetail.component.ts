import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mmproductdetail',
  templateUrl: './mmproductdetail.component.html',
  styleUrls: ['./mmproductdetail.component.scss'],
})
export class MmproductdetailComponent implements OnInit {
  category: any;

  constructor() { }

  ngOnInit() {
    this.category=[
      { "priceone":"500","pricetwo":"1000", "nameone":"saree",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/09/Kayce-Trendz-Kasmeera-Kilkaari-Vol-2-Modal-Silk-Fancy-Dress-Materials-Supplier-1-e1569915883507-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg","imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      {  "priceone":"600","pricetwo":"1000", "nameone":"Kurtis",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/09/Kayce-Trendz-Kasmeera-Kilkaari-Vol-2-Modal-Silk-Fancy-Dress-Materials-Supplier-1-e1569915883507-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      { "priceone":"700","pricetwo":"1000","nameone":"Salwar Suits",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/09/Kayce-Trendz-Kasmeera-Kilkaari-Vol-2-Modal-Silk-Fancy-Dress-Materials-Supplier-1-e1569915883507-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      {"priceone":"800","pricetwo":"1000", "nameone":"Lehenga",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/09/Kayce-Trendz-Kasmeera-Kilkaari-Vol-2-Modal-Silk-Fancy-Dress-Materials-Supplier-1-e1569915883507-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      { "priceone":"900","pricetwo":"1000", "nameone":"Party Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/09/Kayce-Trendz-Kasmeera-Kilkaari-Vol-2-Modal-Silk-Fancy-Dress-Materials-Supplier-1-e1569915883507-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      { "priceone":"950","pricetwo":"1000", "nameone":"Handloom Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/09/Kayce-Trendz-Kasmeera-Kilkaari-Vol-2-Modal-Silk-Fancy-Dress-Materials-Supplier-1-e1569915883507-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    ]
  }

}
