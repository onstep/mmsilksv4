import { ExampleComponent } from './example/example.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MarkdownModule } from 'ngx-markdown';
import {
  FivethreeCoreModule,
  FivIconModule,
  FivBackButtonModule,
  FivGalleryModule
} from '@fivethree/core';
import { MatTooltipModule, MatTabsModule } from '@angular/material';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LottieAnimationViewModule } from 'ng-lottie';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DocsFooterComponent } from './docs-footer/docs-footer.component';
import { ModalPage } from './modal/modal.page';
import { MmswipeComponent } from './mmswipe/mmswipe.component';
import { MmcategoryComponent } from './mmcategory/mmcategory.component';
import { MmfeaturedComponent } from './mmfeatured/mmfeatured.component';
import { MmproductlistComponent } from './mmproductlist/mmproductlist.component';
import { MmproductdetailComponent } from './mmproductdetail/mmproductdetail.component';
import { ForproductlistComponent } from './forproductlist/forproductlist.component';
import { MmsubcategoryComponent } from './mmsubcategory/mmsubcategory.component';
import { MmcartComponent } from './mmcart/mmcart.component';
import { MmwishComponent } from './mmwish/mmwish.component';
import { MmforcategoryComponent } from './mmforcategory/mmforcategory.component';
import { MmforsubcategoryComponent } from './mmforsubcategory/mmforsubcategory.component';
import { MmforfeatureComponent } from './mmforfeature/mmforfeature.component';
import { MmloaderComponent } from './mmloader/mmloader.component';
import { MmpaymentloadingComponent } from './mmpaymentloading/mmpaymentloading.component';
import { MmnavbarComponent } from './mmnavbar/mmnavbar.component';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
export function playerFactory() {
  return player;
}
const components = [
  ExampleComponent,
  HeaderComponent,
  FooterComponent,
  DocsFooterComponent,
  ModalPage,
  MmfeaturedComponent,
  MmswipeComponent,
  MmcategoryComponent,
  MmfeaturedComponent,
  MmcategoryComponent,
  MmproductlistComponent,
  MmproductdetailComponent,
  MmsubcategoryComponent,
  MmcartComponent,
  MmwishComponent,
  ForproductlistComponent,
  MmsubcategoryComponent,
  MmforcategoryComponent,
  MmforsubcategoryComponent,
  MmforfeatureComponent,
  MmloaderComponent,
  MmnavbarComponent,
  MmpaymentloadingComponent

];
@NgModule({
  declarations: components,
  imports: [
    CommonModule,
    RouterModule,
    IonicModule,
    FormsModule,
    MarkdownModule.forChild(),
    MatTooltipModule,
    MatTabsModule,
    LottieAnimationViewModule,
    FivIconModule,
    FivBackButtonModule,
    FivGalleryModule,
    LottieAnimationViewModule.forRoot(),

  ],
  exports: [...components, LottieAnimationViewModule, MarkdownModule],
  entryComponents: [ModalPage]
})
export class ComponentsModule {}
