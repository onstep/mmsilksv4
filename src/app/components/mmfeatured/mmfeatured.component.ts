import { Component, OnInit,Input } from '@angular/core';
import { Storage } from '@ionic/storage';
// import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mmfeatured',
  templateUrl: './mmfeatured.component.html',
  styleUrls: ['./mmfeatured.component.scss'],
})
export class MmfeaturedComponent implements OnInit {
  category:any;
  @Input('data') data: any;

  // @Input('id') id: any;
  // @Input('name') name: any;
  // @Input('image') image: any;
  // @Input('price') price: any;

  constructor(private storage: Storage,private router: Router) { }

  ngOnInit() {
    // this.category=[
    //   { "price":"500", "nameone":"saree",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg","imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"600", "nameone":"Kurtis",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"700", "nameone":"Salwar Suits",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"800", "nameone":"Lehenga",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"900", "nameone":"Party Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"800", "nameone":"Handloom Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    // ]
    // console.log(this.data);
   

  }

productdetail(id){
  this.router.navigateByUrl('/mmproductdetails/'+id)

}
}