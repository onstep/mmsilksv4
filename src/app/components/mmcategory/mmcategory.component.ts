import { Component, OnInit,Input,ViewChild, } from '@angular/core';
import { Router } from '@angular/router';
import {GlobalService } from '../../services/global.service';
 
@Component({
  selector: 'app-mmcategory',
  templateUrl: './mmcategory.component.html',
  styleUrls: ['./mmcategory.component.scss'],
})
export class MmcategoryComponent implements OnInit {
  category:any;

  @Input('id') id: any;
  @Input('name') name: any;
  @Input('img') img: any;
  
  constructor(private router: Router,public global: GlobalService) { }

  ngOnInit() {
  }
  subcategories(id,name){
    console.log(name);
    this.router.navigateByUrl('/mmsubategory/'+id+'/'+name)
  }
  
  }

