import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmwishComponent } from './mmwish.component';

describe('MmwishComponent', () => {
  let component: MmwishComponent;
  let fixture: ComponentFixture<MmwishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmwishComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmwishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
