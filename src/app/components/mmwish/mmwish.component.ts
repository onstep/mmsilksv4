import { Component, OnInit,Input } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-mmwish',
  templateUrl: './mmwish.component.html',
  styleUrls: ['./mmwish.component.scss'],
})
export class MmwishComponent implements OnInit {
  @Input('id') id: any;
  @Input('img') img: any;
  @Input('name') name: any;
  @Input('price') price: any;
  @Input('delprice') delprice: any;
  @Input('description') description: any;
  count: any;
  loaded:any=0;
  heart:any;
 
  constructor(private storage: Storage,public global: GlobalService) { }

  ngOnInit() {

  }
  delete(){

    ///retrive the wishlist data from localstorage
  this.storage.get('wishlist').then((val) => {
//console.log(val);
var  index = val.findIndex((obj => obj.productid == this.id));
// alert(index);
console.log(val);


//if already in list 
if(index!=-1){

  val.splice(index, 1);
  this.global.wishing.product.splice(index, 1);


  console.log(val);
  this.storage.set('wishlist',val).then((val) => {
    // window.location.reload();
   
    if(val.length==0){
      this.global.heart='none';
    }
    console.log(this.global.heart);

  });

}

});

 }
 

 addcart(){
 
  this.storage.get('storecart').then((val) => {

  var  index = val.findIndex((obj => obj.productid == this.id));
  //alert(index);
  if(index== -1){
    let item ={productid: this.id, price: this.price,count:1};



val.push(item),
this.storage.set('storecart',val).then((val) => {
var count;
this.storage.get('storecart').then((vall) => {
//  console.log(vall);
  var  index = vall.findIndex((obj => obj.productid ==  this.id));

   if(index == -1){
   count=0;
  
  }
  else{
 count=vall[index].count;


  }

this.global= count;

  });
this.global.storecart();

});

this.global.storecart();

var  index = val.findIndex((obj => obj.productid ==  this.id));
//val[index].count++;
//this.storage.set('storecart',val);
// alert(val[index].count);



  }else{
    val[index].count++;
    this.storage.set('storecart',val).then((val) => {
      var count;
      this.storage.get('storecart').then((vall) => {
     //   console.log(vall);
        var  index = vall.findIndex((obj => obj.productid ==  this.id));
      
         if(index == -1){
         count=0;
        
        }
        else{
       count=vall[index].count;
  
        }
      
     this.count= count;
  
        });
      this.global.storecart();
      
      });
  }
  });
 this.global.storecart();

 
this.delete();


}
}