import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-mmpaymentloading',
  templateUrl: './mmpaymentloading.component.html',
  styleUrls: ['./mmpaymentloading.component.scss'],
})
export class MmpaymentloadingComponent implements OnInit {

  constructor(public loadingController: LoadingController) { }

  ngOnInit() {}
  async placeorder(){
   
    const loading = await this.loadingController.create({
      message: 'Please Wait a Second',
      duration: 3000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  
  }
}
