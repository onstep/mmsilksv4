import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmpaymentloadingComponent } from './mmpaymentloading.component';

describe('MmpaymentloadingComponent', () => {
  let component: MmpaymentloadingComponent;
  let fixture: ComponentFixture<MmpaymentloadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmpaymentloadingComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmpaymentloadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
