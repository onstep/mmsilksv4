import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
   { path: '', redirectTo: 'myhome/tabs/tab1', pathMatch: 'full' },
  //{ path: '', redirectTo: 'orderdetails/4', pathMatch: 'full' },

//   { 
//     path: '', loadChildren: './pages/mmhome/mmhome.module#MmhomePageModule' 
//   }
// ,
  {
    path: 'getting-started',
    loadChildren:
      './pages/getting-started/getting-started.module#GettingStartedPageModule'
  },
  {
    path: 'buttons',
    loadChildren: './pages/buttons/buttons.module#ButtonsPageModule'
  },
  {
    path: 'components',
    loadChildren: './pages/components/components.module#ComponentsPageModule'
  },
  {
    path: 'bottom-sheet',
    loadChildren:
      './pages/bottom-sheet/bottom-sheet.module#BottomSheetPageModule'
  },
  {
    path: 'loading',
    loadChildren: './pages/loading/loading.module#LoadingPageModule'
  },
  {
    path: 'password',
    loadChildren: './pages/password/password.module#PasswordPageModule'
  },
  {
    path: 'toolbar-search',
    loadChildren:
      './pages/toolbar-search/toolbar-search.module#ToolbarSearchPageModule'
  },
  {
    path: 'app-bar',
    loadChildren: './pages/app-bar/app-bar.module#AppBarPageModule'
  },
  {
    path: 'viewport',
    loadChildren: './pages/viewport/viewport.module#ViewportPageModule'
  },
  { path: 'icon', loadChildren: './pages/icon/icon.module#IconPageModule' },
  {
    path: 'dialog',
    loadChildren: './pages/dialog/dialog.module#DialogPageModule'
  },
  { path: 'image', loadChildren: './pages/image/image.module#ImagePageModule' },
  {
    path: 'refresh',
    loadChildren: './pages/refresh/refresh.module#RefreshPageModule'
  },
  {
    path: 'expandable',
    loadChildren: './pages/expandable/expandable.module#ExpandablePageModule'
  },
  {
    path: 'stepper',
    loadChildren: './pages/stepper/stepper.module#StepperPageModule'
  },
  {
    path: 'stepper/:id',
    loadChildren: './pages/stepper/stepper.module#StepperPageModule'
  },
  {
    path: 'directives',
    loadChildren: './pages/directives/directives.module#DirectivesPageModule'
  },
  {
    path: 'editable-label',
    loadChildren:
      './pages/editable-label/editable-label.module#EditableLabelPageModule'
  },
  {
    path: 'feature-discovery',
    loadChildren:
      './pages/feature-discovery/feature-discovery.module#FeatureDirectoryPageModule'
  },
  {
    path: 'installation',
    loadChildren:
      './pages/installation/installation.module#InstallationPageModule'
  },
  {
    path: 'developer-tools',
    loadChildren:
      './pages/developer-tools/developer-tools.module#DeveloperToolsPageModule'
  },
  {
    path: 'backbutton',
    loadChildren: './pages/backbutton/backbutton.module#BackButtonPageModule'
  },
  { path: 'fab', loadChildren: './pages/fab/fab.module#FabPageModule' },
  { path: 'test', loadChildren: './test/test.module#TestPageModule' },
  /*
  {
    path: '**',
    loadChildren:
      './pages/page-not-found/page-not-found.module#PageNotFoundPageModule'
  },
  */
  { path: 'map', loadChildren: './pages/map/map.module#MapPageModule' },
  { path: 'addresspicker', loadChildren: './pages/addresspicker/addresspicker.module#AddresspickerPageModule' },
  { path: 'things', loadChildren: './pages/things/things.module#ThingsPageModule' },
  { path: 'estimate', loadChildren: './pages/estimate/estimate.module#EstimatePageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'confirm', loadChildren: './pages/confirm/confirm.module#ConfirmPageModule' },
  { path: 'success', loadChildren: './pages/success/success.module#SuccessPageModule' },
  { path: 'success/:orderid', loadChildren: './pages/success/success.module#SuccessPageModule' },
  { path: 'mybookings', loadChildren: './pages/mybookings/mybookings.module#MybookingsPageModule' },
  { path: 'bookingdetails', loadChildren: './pages/bookingdetails/bookingdetails.module#BookingdetailsPageModule' },
  { path: 'bookingdetails/:orderid', loadChildren: './pages/bookingdetails/bookingdetails.module#BookingdetailsPageModule' },
  { path: 'confirmtruck', loadChildren: './pages/confirmtruck/confirmtruck.module#ConfirmtruckPageModule' },
  { path: 'orderdetails', loadChildren: './pages/orderdetails/orderdetails.module#OrderdetailsPageModule' },
  { path: 'orderdetails/:orderid', loadChildren: './pages/orderdetails/orderdetails.module#OrderdetailsPageModule' },
  { path: 'myhome', loadChildren: './pages/myhome/myhome.module#MyhomePageModule' },
  { path: 'mmsubategory', loadChildren: './pages/mmsubategory/mmsubategory.module#MmsubategoryPageModule' },
  { path: 'mmsubategory/:id/:name', loadChildren: './pages/mmsubategory/mmsubategory.module#MmsubategoryPageModule' },
  { path: 'mmproductlisting', loadChildren: './pages/mmproductlisting/mmproductlisting.module#MmproductlistingPageModule' },
  { path: 'mmproductlisting/:id/:name', loadChildren: './pages/mmproductlisting/mmproductlisting.module#MmproductlistingPageModule' },
  { path: 'mmproductdetails', loadChildren: './pages/mmproductdetails/mmproductdetails.module#MmproductdetailsPageModule' },
  { path: 'mmproductdetails/:id', loadChildren: './pages/mmproductdetails/mmproductdetails.module#MmproductdetailsPageModule' },
  { path: 'mmnewcarts', loadChildren: './pages/mmnewcarts/mmnewcarts.module#MmnewcartsPageModule' },
  { path: 'mmwishlist', loadChildren: './pages/mmwishlist/mmwishlist.module#MmwishlistPageModule' },
  { path: 'mmwishlist/:idarray', loadChildren: './pages/mmwishlist/mmwishlist.module#MmwishlistPageModule' },
  { path: 'category', loadChildren: './pages/category/category.module#CategoryPageModule' },
  { path: 'mmaddress', loadChildren: './pages/mmaddress/mmaddress.module#MmaddressPageModule' },
  { path: 'checkout', loadChildren: './pages/checkout/checkout.module#CheckoutPageModule' },
  { path: 'address', loadChildren: './pages/address/address.module#AddressPageModule' },
  { path: 'mmreferfriend', loadChildren: './pages/mmreferfriend/mmreferfriend.module#MmreferfriendPageModule' },
  { path: 'mmwallet', loadChildren: './pages/mmwallet/mmwallet.module#MmwalletPageModule' },
  { path: 'mmpreorder', loadChildren: './pages/mmpreorder/mmpreorder.module#MmpreorderPageModule' },
  { path: 'mmsucess/:orderid', loadChildren: './pages/mmsucess/mmsucess.module#MmsucessPageModule' },
  { path: 'mmsearch', loadChildren: './pages/mmsearch/mmsearch.module#MmsearchPageModule' },
  { path: 'myorders', loadChildren: './pages/myorders/myorders.module#MyordersPageModule' },
  { path: 'mmprofile', loadChildren: './pages/mmprofile/mmprofile.module#MmprofilePageModule' },
  { path: 'contactus', loadChildren: './pages/contactus/contactus.module#ContactusPageModule' },
  { path: 'share', loadChildren: './pages/share/share.module#SharePageModule' },  { path: 'addaddress', loadChildren: './pages/addaddress/addaddress.module#AddaddressPageModule' },





];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
