import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
from:any;
to:any;
items:any;
data: any;
userid: any;
mobile: any;
status: any;
state: any='0';
flack: any;
wallet: any;
cart: any;
total: any='0';
remove: any="0";
wishing:any;
heart:any;
price:any;
carting:any;
ordering:any;
customerid:any;
customerrole:any;
in: any;
payment:any=[];
useraddress:any=[];
productinacrtcount: any;
storecartdetails: any=[];
allproductcount:any=[];
mappageform:any={};
mapstyle:any=[
  {
      "featureType": "all",
      "elementType": "labels.text.fill",
      "stylers": [
          {
              "color": "#7c93a3"
          },
          {
              "lightness": "-10"
          }
      ]
  },
  {
      "featureType": "administrative.country",
      "elementType": "geometry",
      "stylers": [
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "administrative.country",
      "elementType": "geometry.stroke",
      "stylers": [
          {
              "color": "#a0a4a5"
          }
      ]
  },
  {
      "featureType": "administrative.province",
      "elementType": "geometry.stroke",
      "stylers": [
          {
              "color": "#62838e"
          }
      ]
  },
  {
      "featureType": "landscape",
      "elementType": "geometry.fill",
      "stylers": [
          {
              "color": "#dde3e3"
          }
      ]
  },
  {
      "featureType": "landscape.man_made",
      "elementType": "geometry.stroke",
      "stylers": [
          {
              "color": "#3f4a51"
          },
          {
              "weight": "0.30"
          }
      ]
  },
  {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "simplified"
          }
      ]
  },
  {
      "featureType": "poi.attraction",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "poi.business",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "poi.government",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "poi.park",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "poi.place_of_worship",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "poi.school",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "poi.sports_complex",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "road",
      "elementType": "all",
      "stylers": [
          {
              "saturation": "-100"
          },
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "road.highway",
      "elementType": "geometry.fill",
      "stylers": [
          {
              "color": "#bbcacf"
          }
      ]
  },
  {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
          {
              "lightness": "0"
          },
          {
              "color": "#bbcacf"
          },
          {
              "weight": "0.50"
          }
      ]
  },
  {
      "featureType": "road.highway",
      "elementType": "labels",
      "stylers": [
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "road.highway",
      "elementType": "labels.text",
      "stylers": [
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.fill",
      "stylers": [
          {
              "color": "#ffffff"
          }
      ]
  },
  {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.stroke",
      "stylers": [
          {
              "color": "#a9b4b8"
          }
      ]
  },
  {
      "featureType": "road.arterial",
      "elementType": "labels.icon",
      "stylers": [
          {
              "invert_lightness": true
          },
          {
              "saturation": "-7"
          },
          {
              "lightness": "3"
          },
          {
              "gamma": "1.80"
          },
          {
              "weight": "0.01"
          }
      ]
  },
  {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
          {
              "color": "#a3c7df"
          }
      ]
  }
];
dbloaded:any=0;
constructor(public storage: Storage,public http: HttpClient, ) { 

    this.storage.get('userid').then((val) => {
        //  alert(val);
          if(val!=null){
              this.state=1;
            
            this.customerid=val;
        //    alert(this.customerid);
          }
    
        });


        this.storage.get('role').then((val) => {
            //  alert(val);
              if(val!=null){
                
                this.customerrole=val;
               
              }
        
            });

            this.storage.get('address').then((val) => {
                //  alert(val);
                  if(val!=null){
                    
                    this.useraddress=val;
                   
                  }
            
                });

  this.storage.get('fromaddress').then((fromaddress) => {
    if(fromaddress){
      this.from=fromaddress;
    }
    this.storage.get('toaddress').then((toaddress) => {
      if(toaddress){
        this.to=toaddress;
      }
      this.storage.get('mappageform').then((mappageform) => {
        if(mappageform){
          console.log('form'+mappageform);
          console.log(mappageform);
          this.mappageform=mappageform;
        }
    this.dbloaded=1;
      });
        });
  });
  this.storage.get('storecart').then((val) => {
    //  alert(val);
      if(val==null){
        var storecartdata=[];
          this.storage.set('storecart', storecartdata);
       
      }


        
        this.storecart();


  
    });
}
url = 'http://13.232.62.63/bigcart/mmsilksbackend/home/';
  otp(mobile){

    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData =  { userid: mobile}

    this.http.post(this.url+"login/otp.php", postData,{observe: 'response'})
      .subscribe(data => {
        console.log(data);
 return data;
       }, error => {
        console.log(error);
      });

}

getstatus(){
    this.storage.get('userdata').then((val) => {
return val.status;
     });
}
adding(){
    this.total='0';
    for(var i=0; i<this.price.length; i++)
    {
    this.total=parseInt(this.total)+parseInt(this.price[i].price);
    
    }
  }

removestorecart(){

 var storecartdata=[];

  this.storage.set('storecart', storecartdata)
  .then(
   () => 
 this.storage.get('storecart').then((val) => {
   
   this.storecartdetails=val;
   this.storecart();
 })

 );

}

 storecart(){

   this.storage.get('storecart').then((val) => {
     console.log(val);
   //alert(val.length);
 var total:number=0;
 var totalmrp:number=0;
 var totalcount:number=0;

for(var i=0;i<val.length; i++){
       total=total + (val[i].price * val[i].count);
       totalcount=totalcount+val[i].count;
       console.log(totalcount);
        }
for(var i=0;i<val.length; i++){
       totalmrp=totalmrp + (val[i].mrp * val[i].count);
       totalcount=totalcount+val[i].count;
       console.log(totalcount);
        }
                this.storecartdetails.all=val;
                this.storecartdetails.price=total;
                this.storecartdetails.mrp=totalmrp;
                this.storecartdetails.totalcount=totalcount;
                this.storecartdetails.count=val.length;
                console.log(this.storecartdetails.count);

    // alert("price"+this.storecartdetails.price);
     //alert("price"+this.storecartdetails.count);
     //alert("price"+this.storecartdetails.totalcount);
   });
  
      
       
     
   
     }
   


 productcount(productid){

   this.storage.get('storecart').then((val) => {
 
 var  index = val.findIndex((obj => obj.productid == productid));
 
  if(index == -1){
   this.productinacrtcount[productid]=0;
 
 }
 else{
   this.productinacrtcount[productid]=val[index].count;
 
 }
 });
  }
 
addtostorecart(productid,price,mrp){

   this.storage.get('storecart').then((val) => {

   var  index = val.findIndex((obj => obj.productid == productid));
   //alert(index);
   if(index== -1){
  let item ={productid: productid, price: price,mrp:mrp,count:1};
 

 val.push(item),
this.storage.set('storecart',val).then((val) => {
this.storecart();

});
 
 this.storecart();

 var  index = val.findIndex((obj => obj.productid == productid));
 //val[index].count++;
 //this.storage.set('storecart',val);
// alert(val[index].count);



   }else{
     val[index].count++;
     this.storage.set('storecart',val).then((val) => {
       this.storecart();
       
       });
   }
   });
   this.storecart();




 }

reduceinstorecart(productid){
var count; 
   this.storage.get('storecart').then((val) => {

   var  index = val.findIndex((obj => obj.productid == productid));
   //alert(index);
  
     val[index].count--;
     if(val[index].count==0){
       val.splice(index,1);
     }
     this.storage.set('storecart',val).then((val) => {





       this.storage.get('storecart').then((val) => {
         console.log(val);
         var  index = val.findIndex((obj => obj.productid == productid));
       
          if(index == -1){
          count=0;
         
         }
         else{
        count=val[index].count;

         }
        // alert(count+'jjj');
      this.allproductcount[productid]= count;

         });






       this.storecart();
       




       });
   });
   this.storecart();
      
 }
 
 




  
    

     updatecart(productid,price,mrp){

       this.storage.get('storecart').then((val) => {

         var  index = val.findIndex((obj => obj.productid == productid));
         //alert(index);
         if(index== -1){
        let item ={productid: productid, price: price,mrp:mrp,count:1};
       
     
       val.push(item),
     this.storage.set('storecart',val).then((val) => {
     this.storecart();
     
     });
       
       this.storecart();
     
       var  index = val.findIndex((obj => obj.productid == productid));
       //val[index].count++;
       //this.storage.set('storecart',val);
     // alert(val[index].count);
     
     
     
         }else{
           val[index].price=price;
           val[index].mrp=mrp;
           this.storage.set('storecart',val).then((val) => {
             this.storecart();
             
             });
         }
         });
         this.storecart();
     
     
     
     
       }
}
