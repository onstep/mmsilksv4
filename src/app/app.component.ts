import { Component } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FivRoutingStateService } from '@fivethree/core';
import { AndroidFullScreen } from '@ionic-native/android-full-screen/ngx';
import { Storage } from '@ionic/storage';

import { GlobalService } from './services/global.service';


//declare var $: any;
import * as $ from 'jquery';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public components = [
    {
      title: 'App Bar',
      url: '/app-bar',
      icon: 'git-commit'
    },
    {
      title: 'Back Button',
      url: '/backbutton',
      icon: 'md-arrow-back'
    },
    {
      title: 'Bottom Sheet',
      url: '/bottom-sheet',
      icon: 'ios-arrow-up'
    },
    {
      title: 'Dialog',
      url: '/dialog',
      icon: 'notifications'
    },
    {
      title: 'Editable Label',
      url: '/editable-label',
      icon: 'create'
    },
    {
      title: 'Expandable',
      url: '/expandable',
      icon: 'resize'
    },
    {
      title: 'FAB',
      url: '/fab',
      icon: 'add-circle'
    },
    {
      title: 'Icon',
      url: '/icon',
      icon: 'mail-unread'
    },
    {
      title: 'Image Gallery',
      url: '/image',
      icon: 'image'
    },
    {
      title: 'Loading Indicators',
      url: '/loading',
      icon: 'time'
    },
    {
      title: 'Overflow Buttons',
      url: '/buttons',
      icon: 'more'
    },
    {
      title: 'Password Reveal Input',
      url: '/password',
      icon: 'key'
    },
    {
      title: 'Refresher',
      url: '/refresh',
      icon: 'refresh'
    },
    {
      title: 'Searchbar',
      url: '/toolbar-search',
      icon: 'search'
    },
    {
      title: 'Stepper',
      url: '/stepper',
      icon: 'share'
    }
  ];
  public directives = [
    {
      title: 'Feature Discovery',
      url: '/feature-discovery',
      icon: 'resize'
    },
    {
      title: 'Viewport',
      url: '/viewport',
      icon: 'share'
    }
  ];

  public services = [];

  constructor(
    public global: GlobalService , public storage:Storage,
    private androidFullScreen: AndroidFullScreen,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private routing: FivRoutingStateService,
    private navCtrl: NavController,
    private oneSignal: OneSignal,
   
  ) {
    this.initializeApp();
    this.declarestorecart();
  }


ngAfterViewInit() {
    // This element never changes.
    let ionapp = document.getElementsByTagName("ion-app")[0];

    window.addEventListener('keyboardDidShow', async (event) => {
     // alert($(window).height());
        // Move ion-app up, to give room for keyboard
        let kbHeight: number = event["keyboardHeight"];
        let viewportHeight: number = $(window).height();
        let inputFieldOffsetFromBottomViewPort: number = viewportHeight - $(':focus')[0].getBoundingClientRect().bottom;
        let inputScrollPixels = kbHeight - inputFieldOffsetFromBottomViewPort;
console.log(inputFieldOffsetFromBottomViewPort);
console.log('inputScrollPixels');

console.log(inputScrollPixels);
        // Set margin to give space for native keyboard.
        ionapp.style["margin-bottom"] = kbHeight.toString() + "px";

        // But this diminishes ion-content and may hide the input field...
        if (inputScrollPixels > 0) {
            // ...so, get the ionScroll element from ion-content and scroll correspondingly
            // The current ion-content element is always the last. If there are tabs or other hidden ion-content elements, they will go above.
            let ionScroll = await $("ion-content").last()[0].getScrollElement();
            setTimeout(() => {
                $(ionScroll).animate({
                    scrollTop: ionScroll.scrollTop + inputScrollPixels + 200
                }, 300);
            }, 300); // Matches scroll animation from css.
        }
    });
    window.addEventListener('keyboardDidHide', () => {
        // Move ion-app down again
        // Scroll not necessary.
        ionapp.style["margin-bottom"] = "0px";
    });
}

  initializeApp() {
    this.platform.ready().then(() => {
      this.routing.loadRouting({ clearOn: ['/'], root: '/' });
      // this.androidFullScreen.showSystemUI()
      // .then(() => console.log('Immersive mode supported'))
      // .catch(err => console.log(err));



      /////notifiaction
      var mobile=1;
if(mobile=1){


      this.oneSignal.startInit('63b1c2bf-e39f-4929-af6c-28533e1d271b', '253101300565');

this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

this.oneSignal.handleNotificationReceived().subscribe(() => {
 // do something when notification is received
 
});

this.oneSignal.handleNotificationOpened().subscribe(() => {
  // do something when a notification is opened
});

this.oneSignal.endInit();
}

      //this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#ffffff');

      // this.androidFullScreen.immersiveMode()
      // .then(() => console.log('Immersive mode supported'))
      // .catch(err => console.log(err));
    
      this.splashScreen.hide();
    });
  }

  navigateRoot(url: string) {
    this.navCtrl.navigateRoot(url);
  }
  navigate(url: string) {
    this.navCtrl.navigateForward(url);
  }

  declarestorecart(){
  
    //this.storage.remove('storecart');
    ////////////if storecart is null declare the empty array
      this.storage.get('storecart').then((val) => {
      //  alert(val);
        if(val==null){
          const storecartdata=[];
            this.storage.set('storecart', storecartdata);

    }
   });
    ////////////if wishlist is null declare the empty array

   this.storage.get('wishlist').then((val) => {
      //  alert(val);
        if(val==null){
          const wishlistdata=[];//declaring empty array 
            this.storage.set('wishlist', wishlistdata);

    }
   });


  }
}